-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for diabetesapp
CREATE DATABASE IF NOT EXISTS `diabetesapp` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `diabetesapp`;

-- Dumping structure for table diabetesapp.diabetes
CREATE TABLE IF NOT EXISTS `diabetes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `waktu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenisPemeriksaan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nilai` double DEFAULT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci,
  `oleh` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idUser` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.diabetes: ~13 rows (approximately)
/*!40000 ALTER TABLE `diabetes` DISABLE KEYS */;
INSERT INTO `diabetes` (`id`, `waktu`, `jenisPemeriksaan`, `nilai`, `catatan`, `oleh`, `idUser`, `created_at`, `updated_at`) VALUES
	(6, 'Sore', 'HbA1c', 40, 'Normal', 'Maria', '9', '2024-02-12 13:23:58', '2024-02-12 13:23:58'),
	(7, 'Pagi', 'Gula Darah Puasa', 10, 'Sesuai', 'admin', '5', '2024-02-13 09:59:01', '2024-02-13 09:59:01'),
	(8, 'Pagi', 'Gula Darah Sewaktu', 10, 'Test', 'Test', '17', '2024-02-13 10:16:40', '2024-02-13 10:16:40'),
	(10, 'Sore', 'HbA1c', 90, 'Normal', 'Maria', '9', '2024-02-13 17:52:55', '2024-02-13 17:52:55'),
	(11, 'Sore', 'Gula Darah Sewaktu', 95, 'Normal', 'Maria', '9', '2024-02-13 18:08:11', '2024-02-13 18:08:11'),
	(12, 'Siang', 'Gula Darah Puasa', 12, 'Tidak ada', 'admin', '5', '2024-02-13 18:09:23', '2024-02-13 18:09:23'),
	(19, 'Pagi', 'Gula Darah Sewaktu', 1, NULL, 'admin', '5', '2024-02-18 18:11:26', '2024-02-18 18:11:26'),
	(20, 'Pagi', 'GD 2 Jam Setelah Makan', 1, NULL, 'admin', '5', '2024-02-18 19:18:14', '2024-02-18 19:18:14'),
	(21, 'Siang', 'Gula Darah Sewaktu', 121, 'test', 'admin', '5', '2024-02-24 18:42:00', '2024-02-24 14:44:49'),
	(22, 'Sore', 'HbA1c', 1000, NULL, 'Maria', '9', '2024-02-23 19:52:00', '2024-02-24 19:52:54'),
	(23, 'Pagi', 'Gula Darah Puasa', 123, NULL, 'Ibu Rokxxxxx', '19', '2024-02-01 20:53:00', '2024-02-25 20:53:47'),
	(24, 'Pagi', 'Gula Darah Sewaktu', 8, 'Test', 'Yedi Setiadi', '20', '2024-03-01 00:00:00', '2024-03-01 16:52:24'),
	(25, 'Pagi', 'Gula Darah Sewaktu', 234, NULL, 'Juju', '22', '2024-03-02 00:00:00', '2024-03-02 08:58:22');
/*!40000 ALTER TABLE `diabetes` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.diskusis
CREATE TABLE IF NOT EXISTS `diskusis` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `diskusi` text,
  `oleh` varchar(255) DEFAULT NULL,
  `idUser` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table diabetesapp.diskusis: ~0 rows (approximately)
/*!40000 ALTER TABLE `diskusis` DISABLE KEYS */;
INSERT INTO `diskusis` (`id`, `diskusi`, `oleh`, `idUser`, `created_at`, `updated_at`) VALUES
	(1, 'Tes', 'admin', '5', '2024-02-13 22:47:51', '2024-02-13 22:47:51'),
	(2, 'Percakapan', 'Maria', '9', '2024-02-14 10:01:44', '2024-02-14 10:01:44'),
	(4, 'Haloo admin mau tanya boleh?', 'Maria', '9', '2024-02-15 10:34:11', '2024-02-15 10:34:11'),
	(5, 'Diabet', 'Test', '17', '2024-02-15 16:22:36', '2024-02-15 16:22:36'),
	(6, 'Di rincikan Pertanyaannya', 'Administrator', '14', '2024-02-15 16:23:34', '2024-02-15 16:23:34'),
	(7, 'Test diskusi', 'Maria', '9', '2024-02-17 18:20:11', '2024-02-17 18:20:11'),
	(8, 'Tanya dokter', 'Maria', '9', '2024-02-24 19:54:10', '2024-02-24 19:54:10');
/*!40000 ALTER TABLE `diskusis` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.food
CREATE TABLE IF NOT EXISTS `food` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `picturePath` text COLLATE utf8mb4_unicode_ci,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `ingredients` text COLLATE utf8mb4_unicode_ci,
  `price` int(11) DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `types` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.food: ~0 rows (approximately)
/*!40000 ALTER TABLE `food` DISABLE KEYS */;
/*!40000 ALTER TABLE `food` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.lukas
CREATE TABLE IF NOT EXISTS `lukas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `picturePath` text COLLATE utf8mb4_unicode_ci,
  `catatan` text COLLATE utf8mb4_unicode_ci,
  `oleh` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `idUser` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.lukas: ~2 rows (approximately)
/*!40000 ALTER TABLE `lukas` DISABLE KEYS */;
INSERT INTO `lukas` (`id`, `picturePath`, `catatan`, `oleh`, `idUser`, `created_at`, `updated_at`) VALUES
	(15, 'assets/user/3K7AbIcGpN0DZEwKYqKjqh1ELkgmGO8lhXvlIF9j.jpg', '11', 'admin', '5', '2024-02-22 14:57:00', '2024-02-24 14:58:01'),
	(17, 'assets/user/waOlvpJLKCyswTZAFvkMxAKf5GEodyCLV0QfzFG1.jpg', 'Nanah sudah tak ada, bengkak rak ada', 'Juju', '22', '2024-02-02 08:58:00', '2024-03-02 09:00:12');
/*!40000 ALTER TABLE `lukas` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.master_quizzes
CREATE TABLE IF NOT EXISTS `master_quizzes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pertanyaankuis` text,
  `jawaban` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table diabetesapp.master_quizzes: ~3 rows (approximately)
/*!40000 ALTER TABLE `master_quizzes` DISABLE KEYS */;
INSERT INTO `master_quizzes` (`id`, `pertanyaankuis`, `jawaban`, `created_at`, `updated_at`) VALUES
	(1, 'Ulkus Kaki Diabetik adalah luka pada penderita diabetes.', 'Benar', NULL, NULL),
	(2, 'Nasi untuk penderita Diabetes harus 2 kepal tangan sekali makan.', 'Salah', NULL, NULL),
	(3, 'Bila kita akan minum/suntik obat diabetes sebaiknya makanan sudah tersedia.', 'Benar', NULL, NULL);
/*!40000 ALTER TABLE `master_quizzes` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.migrations: ~12 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
	(4, '2019_08_19_000000_create_failed_jobs_table', 1),
	(5, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(6, '2020_09_17_052150_create_food_table', 1),
	(7, '2020_09_17_052431_create_transactions_table', 1),
	(8, '2020_09_17_054644_create_sessions_table', 1),
	(9, '2020_09_17_064803_add_roles_to_users_table', 1),
	(10, '2020_09_22_105946_change_picture_field', 1),
	(11, '2024_01_07_122324_create_diabetes_table', 1),
	(12, '2024_01_07_122603_create_lukas_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=357 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.personal_access_tokens: ~50 rows (approximately)
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
	(1, 'App\\Models\\User', 1, 'authToken', '2365507a1b8a27bfd031824f38cca459ef4e2e435875b5e2c737aa9379be02d9', '["*"]', NULL, '2024-01-12 04:12:11', '2024-01-12 04:12:11'),
	(2, 'App\\Models\\User', 1, 'authToken', '257de34a0f5e9fd02d9fb21bfc00342ed2c6c8d4d5fb51c3d7435fd3f01692f4', '["*"]', NULL, '2024-01-12 04:12:20', '2024-01-12 04:12:20'),
	(3, 'App\\Models\\User', 1, 'authToken', 'ced3ae3492291b034d687068f3b89d4d331156ef590f2b9cfd2d3148a165b44c', '["*"]', NULL, '2024-01-13 03:09:28', '2024-01-13 03:09:28'),
	(4, 'App\\Models\\User', 1, 'authToken', 'd9b0068348ad9ac1a8dd6ccdb37e41f22d10789b1a09c1f1c2eddc1cde75e9bf', '["*"]', NULL, '2024-01-13 03:09:41', '2024-01-13 03:09:41'),
	(5, 'App\\Models\\User', 1, 'authToken', 'd94b60d93b8e3416562d7dbd39883258150af033f19edf36013895d639740f7b', '["*"]', NULL, '2024-01-13 03:09:49', '2024-01-13 03:09:49'),
	(6, 'App\\Models\\User', 1, 'authToken', 'd93c89097681b07a25a6d672a6db53fc24fe4d0ff3b65cbf3926f6f527b76c65', '["*"]', NULL, '2024-01-13 03:09:51', '2024-01-13 03:09:51'),
	(7, 'App\\Models\\User', 1, 'authToken', 'f09e32c335cadc06faff29b29f8fa2b7ba6ab6567ee96d606541296d1da70d0c', '["*"]', NULL, '2024-01-13 03:11:40', '2024-01-13 03:11:40'),
	(8, 'App\\Models\\User', 1, 'authToken', '177a517cbd6490c94549ce603db952345b1fb1487704d5c752e66231333b3b5b', '["*"]', NULL, '2024-01-13 03:20:45', '2024-01-13 03:20:45'),
	(9, 'App\\Models\\User', 1, 'authToken', 'd17726d022fa635c62c17f93e7d650bac02e778da4715878780cd431edd85df2', '["*"]', NULL, '2024-01-13 04:53:27', '2024-01-13 04:53:27'),
	(10, 'App\\Models\\User', 1, 'authToken', 'a39bf7d6a699214373fc8fb4012c90fb89746adc31850876dcc55fda8ef804e7', '["*"]', NULL, '2024-01-13 04:57:21', '2024-01-13 04:57:21'),
	(11, 'App\\Models\\User', 1, 'authToken', 'ad63fe5468fc805d04eae155b68eabcbed1b23e53c851a0b910ba042f5b4ce43', '["*"]', NULL, '2024-01-13 05:08:44', '2024-01-13 05:08:44'),
	(12, 'App\\Models\\User', 2, 'authToken', 'ad47cc80524cd9d45f03c0dd484b6ffc4d23200181d685be34b3b896206199b1', '["*"]', NULL, '2024-01-13 15:51:33', '2024-01-13 15:51:33'),
	(13, 'App\\Models\\User', 1, 'authToken', '7f4a2bb8632c39c9f5bbd6bd9b5641c31d1b3b994f1b6d07f86a2e27d16b93c2', '["*"]', NULL, '2024-01-14 03:14:01', '2024-01-14 03:14:01'),
	(14, 'App\\Models\\User', 3, 'authToken', '3c44dc9b053bf691674a4709ccf1f2a4d60f350f7d904dd80cef080301bbeaa2', '["*"]', '2024-01-14 17:25:29', '2024-01-14 17:25:28', '2024-01-14 17:25:29'),
	(15, 'App\\Models\\User', 1, 'authToken', '9be21545e9ae6bf32dd819de10e1cb61c4639dbcabf5a8214fd22b8d1bb9fa92', '["*"]', '2024-01-14 17:39:36', '2024-01-14 17:38:07', '2024-01-14 17:39:36'),
	(16, 'App\\Models\\User', 4, 'authToken', 'fc49822fe27cbac3bd7152ade9bad3bf5975445fffecce034929455aedf2cd83', '["*"]', NULL, '2024-01-14 17:56:52', '2024-01-14 17:56:52'),
	(17, 'App\\Models\\User', 5, 'authToken', '2ec6905dd1be9be5d16bcfd00e37cc07d83aff89b00da8adc71ee30d56098ca8', '["*"]', '2024-01-14 18:01:30', '2024-01-14 18:01:30', '2024-01-14 18:01:30'),
	(18, 'App\\Models\\User', 1, 'authToken', '6a238ebe5907781b7b7eba0333b32bd76a82fdc4e464fd597a563e410fdd098b', '["*"]', NULL, '2024-01-14 18:18:14', '2024-01-14 18:18:14'),
	(19, 'App\\Models\\User', 1, 'authToken', '570972921405875ebab6d151557c602bd3764e11c6462f34b9d3a063edf3c2cc', '["*"]', NULL, '2024-01-14 18:22:41', '2024-01-14 18:22:41'),
	(20, 'App\\Models\\User', 1, 'authToken', '5cb0ca5feaa195b0e5eac8eb9a314d0db2c37715420d7d18a3a65cb4b17baa92', '["*"]', NULL, '2024-01-14 18:23:30', '2024-01-14 18:23:30'),
	(21, 'App\\Models\\User', 6, 'authToken', '0ad84928748b97feeb4b1bdd55b8a851eeb818c20eb40f3b35ca1576215dbb47', '["*"]', NULL, '2024-01-14 18:27:41', '2024-01-14 18:27:41'),
	(22, 'App\\Models\\User', 5, 'authToken', '27443008a2dc1ad9088af41de0881430114d40899b4d22c8ea4da125053eb8c5', '["*"]', NULL, '2024-01-14 18:28:26', '2024-01-14 18:28:26'),
	(23, 'App\\Models\\User', 5, 'authToken', '927fa3e55c379683a767ec04ac20ab9c3e3c1a0208cbbfe0985421650960be64', '["*"]', '2024-01-14 18:31:48', '2024-01-14 18:31:10', '2024-01-14 18:31:48'),
	(24, 'App\\Models\\User', 5, 'authToken', 'ceeb7f2bd1e0ca701d1ae0c2d40e71cdf9f834c282b53e96ad31906915724ebe', '["*"]', NULL, '2024-01-14 18:42:49', '2024-01-14 18:42:49'),
	(25, 'App\\Models\\User', 5, 'authToken', '686c394937af03961f07c4b7450b4dba6838211334992c8b937c99e03a03665f', '["*"]', NULL, '2024-01-14 18:45:31', '2024-01-14 18:45:31'),
	(26, 'App\\Models\\User', 5, 'authToken', '7dca77024b399c6738d47d31b71d48e07d685038ed0cd6d047ee295f4cd0d86e', '["*"]', NULL, '2024-01-14 18:49:35', '2024-01-14 18:49:35'),
	(27, 'App\\Models\\User', 5, 'authToken', '996541c30b9705c0d73cee23c30965bf7c6338aa995e68999ae36bc0d8c29c5a', '["*"]', NULL, '2024-01-14 18:53:28', '2024-01-14 18:53:28'),
	(28, 'App\\Models\\User', 5, 'authToken', '6f842e984659190423ecd613fddac07f4124b9c3dafe6bce903e1abe5f302a54', '["*"]', NULL, '2024-01-14 18:58:26', '2024-01-14 18:58:26'),
	(29, 'App\\Models\\User', 5, 'authToken', '518beb442465af76847e4560e63f14c8fa3194c422c86854bc48187a002098c7', '["*"]', NULL, '2024-01-14 19:00:36', '2024-01-14 19:00:36'),
	(30, 'App\\Models\\User', 5, 'authToken', '4e8112c9aa680a70937462955a3862856c23206b8033ebedd1a5f35b1f5df59c', '["*"]', NULL, '2024-01-14 19:08:13', '2024-01-14 19:08:13'),
	(31, 'App\\Models\\User', 5, 'authToken', '8f9824955f032330d3c2db2c6d8b6806c8707827401eb13846e8543d6630613c', '["*"]', NULL, '2024-01-14 19:32:10', '2024-01-14 19:32:10'),
	(32, 'App\\Models\\User', 5, 'authToken', '4fd9b6b0c19b9f7af96e982ccd399c1f507bbcd4b01bc4eec682c67e2c877983', '["*"]', NULL, '2024-01-14 19:33:52', '2024-01-14 19:33:52'),
	(33, 'App\\Models\\User', 5, 'authToken', 'e44cb5629ae299487c5426b4d0a02a848861fe472eb3140b687f9081c530061f', '["*"]', NULL, '2024-01-14 19:34:55', '2024-01-14 19:34:55'),
	(34, 'App\\Models\\User', 5, 'authToken', '367de1e4a91efd5397d95b18a5113c4a7befaf6a0b6aa840dc8ad2d130c92b0d', '["*"]', NULL, '2024-01-14 19:35:31', '2024-01-14 19:35:31'),
	(35, 'App\\Models\\User', 5, 'authToken', '77e2954bb4709df3eb73eb964983767663e8bb320d765951505c3492ed0d8c0d', '["*"]', NULL, '2024-01-14 19:36:28', '2024-01-14 19:36:28'),
	(36, 'App\\Models\\User', 5, 'authToken', 'e78be520538aecea1b76a01d8aaca8a325f53c748e790e18f8769dd306c522c0', '["*"]', NULL, '2024-01-14 19:36:57', '2024-01-14 19:36:57'),
	(37, 'App\\Models\\User', 5, 'authToken', '3cedb0c665aed0f8a8c2e69b6b23e0d33f104c353fc2d55e7aa61f9de32d2f83', '["*"]', NULL, '2024-01-14 19:38:01', '2024-01-14 19:38:01'),
	(38, 'App\\Models\\User', 5, 'authToken', '1fb5d607ca1cf9170d71aee4f941adaadc770b6a133ce28ab0bf5ee49e61854b', '["*"]', NULL, '2024-01-14 19:40:34', '2024-01-14 19:40:34'),
	(39, 'App\\Models\\User', 5, 'authToken', '7b89ca296fe282f42a472d0b4e8c187702e701a884d9501541893d2c1a67785d', '["*"]', NULL, '2024-01-14 19:41:01', '2024-01-14 19:41:01'),
	(40, 'App\\Models\\User', 5, 'authToken', 'da82010fc41af5c043adcb7f00eb21f1a53a8bf516a7359df7ea03c5abc53a04', '["*"]', NULL, '2024-01-14 19:46:41', '2024-01-14 19:46:41'),
	(41, 'App\\Models\\User', 5, 'authToken', '65052b756eb6f8ac4a77e3fde06b22237f1709fe9d9ca7bfded19ff1a63ccc12', '["*"]', NULL, '2024-01-14 19:47:29', '2024-01-14 19:47:29'),
	(42, 'App\\Models\\User', 5, 'authToken', 'fa2920b14098932201950a2803e261c5ecfa8cd63af13873ebdae0619def2194', '["*"]', NULL, '2024-01-14 19:47:58', '2024-01-14 19:47:58'),
	(43, 'App\\Models\\User', 5, 'authToken', 'd2a4015c3f761789eee6260f5656abfa360d85e25390eeeaf0d9aafbb7153740', '["*"]', NULL, '2024-01-15 02:12:00', '2024-01-15 02:12:00'),
	(44, 'App\\Models\\User', 5, 'authToken', 'e93bfd9224d64c93bf9aeaf578a2141917d1ef6aa57e418cf959fe44feae8faa', '["*"]', NULL, '2024-01-15 04:02:37', '2024-01-15 04:02:37'),
	(45, 'App\\Models\\User', 5, 'authToken', '8e562fbadbe71a921c81256e1a5a21863e9fdfedbcb09a0ef8a64347ed458733', '["*"]', NULL, '2024-01-15 04:48:07', '2024-01-15 04:48:07'),
	(46, 'App\\Models\\User', 5, 'authToken', '420c5644e68fa95dd3e426a5211f240ecdcdccfb9e8823a80f33e2f9accaed53', '["*"]', NULL, '2024-01-15 04:55:32', '2024-01-15 04:55:32'),
	(47, 'App\\Models\\User', 5, 'authToken', '00fcb4d99ee8a61216662f8607fa876469e7334a640e942c85878c4fb95864c3', '["*"]', NULL, '2024-01-15 05:29:21', '2024-01-15 05:29:21'),
	(48, 'App\\Models\\User', 5, 'authToken', '76385dc763f4d904afdeba2118436b45ff44ac25c7be8f4f2388ff473b8c8034', '["*"]', NULL, '2024-01-15 05:33:20', '2024-01-15 05:33:20'),
	(49, 'App\\Models\\User', 5, 'authToken', 'ec20ccfd8f21514369e4918127a7170e822b88e5592b2f5c902e374c2b36c924', '["*"]', NULL, '2024-01-15 05:45:00', '2024-01-15 05:45:00'),
	(50, 'App\\Models\\User', 5, 'authToken', '96a31dadfbd65abc8b703d15aee1cfae375e8a40ddb4009b6943525f2359f109', '["*"]', NULL, '2024-01-15 05:45:01', '2024-01-15 05:45:01'),
	(51, 'App\\Models\\User', 5, 'authToken', 'f72c33591a3876f6a0307044d39d5b66169f1aad0139f793c300abbd1f7a9f11', '["*"]', NULL, '2024-01-15 05:48:19', '2024-01-15 05:48:19'),
	(52, 'App\\Models\\User', 5, 'authToken', '909874e2347f8903eb970e4fc2febfe1e336d4e25d0bc4e489831c9800fc155b', '["*"]', NULL, '2024-01-15 05:58:46', '2024-01-15 05:58:46'),
	(53, 'App\\Models\\User', 5, 'authToken', '6b60dd13e2ae2a3ddbec01ed0d105f675397511de91e4db4bb2bc500e7d31d70', '["*"]', NULL, '2024-01-15 06:03:17', '2024-01-15 06:03:17'),
	(54, 'App\\Models\\User', 5, 'authToken', 'ef1d87f514aad36ca12f661fe32a8dd8a19d148706580b4f37030dcc030a7ecf', '["*"]', NULL, '2024-01-15 06:04:24', '2024-01-15 06:04:24'),
	(55, 'App\\Models\\User', 5, 'authToken', 'a54aa848f11b8d5b6f8d3784a3c0175bbe53ca5547b5b05284b52a5f08316117', '["*"]', NULL, '2024-01-15 06:15:48', '2024-01-15 06:15:48'),
	(56, 'App\\Models\\User', 5, 'authToken', '331e8c3bf6ae256db1ad2de921346d72321b0ee84cfc510c87798e914705318d', '["*"]', NULL, '2024-01-15 06:16:20', '2024-01-15 06:16:20'),
	(57, 'App\\Models\\User', 5, 'authToken', 'e67f509a97bcc8a79ff53477f95cfe39ea4cec13c7af4e37f91fc50b66d5c508', '["*"]', NULL, '2024-01-15 06:23:54', '2024-01-15 06:23:54'),
	(58, 'App\\Models\\User', 5, 'authToken', '78585e822ed2a806e12e7794caae6f8b89fdf5342a2ad945179b0b859b53ddc9', '["*"]', NULL, '2024-01-15 06:38:26', '2024-01-15 06:38:26'),
	(59, 'App\\Models\\User', 5, 'authToken', 'bd0710f9cd77bfe95cb0d850002e3c56b0ee70114317d7f57d2553816d90840c', '["*"]', NULL, '2024-01-15 06:40:52', '2024-01-15 06:40:52'),
	(60, 'App\\Models\\User', 5, 'authToken', '7a9fa2b031d0cd3d40a936a29025fcecdea75f8d3c734d5e055abe6f3b8e68a9', '["*"]', NULL, '2024-01-15 07:10:31', '2024-01-15 07:10:31'),
	(61, 'App\\Models\\User', 5, 'authToken', '0bc2b3c2f59658c69963e8c97db3df41f7bf8b9f05e1552bfa369903e94d4b8c', '["*"]', NULL, '2024-01-15 07:14:26', '2024-01-15 07:14:26'),
	(62, 'App\\Models\\User', 5, 'authToken', 'd673551083ef9a84b0843ccf9258f27da1bf8ad9afc8d9e0d0744fe0be1efe3f', '["*"]', NULL, '2024-01-15 07:24:25', '2024-01-15 07:24:25'),
	(63, 'App\\Models\\User', 5, 'authToken', '7a550e2c023f87759327bb102c76173d5efe449adc0e6f50efeadecd720afa94', '["*"]', NULL, '2024-01-15 07:32:54', '2024-01-15 07:32:54'),
	(64, 'App\\Models\\User', 5, 'authToken', '00013759cf2b3408f997ad812a62cccb21b8ff6d6607e01a5c27a376fcccb895', '["*"]', NULL, '2024-01-15 07:33:21', '2024-01-15 07:33:21'),
	(65, 'App\\Models\\User', 5, 'authToken', 'a8214eb3526508835a2f9a8354b94dee47a92ecdf3eb7ea62a210094553c313b', '["*"]', NULL, '2024-01-15 07:34:10', '2024-01-15 07:34:10'),
	(66, 'App\\Models\\User', 5, 'authToken', '0393228c1b02b725660588e4a8551a3d754132d17bceefe50445e8209ab80879', '["*"]', NULL, '2024-01-15 07:36:33', '2024-01-15 07:36:33'),
	(67, 'App\\Models\\User', 5, 'authToken', '037fff51fe14fd14bd0ddfbf6fcc99d8500226e649271badc2691fd67db48aee', '["*"]', NULL, '2024-01-15 07:37:46', '2024-01-15 07:37:46'),
	(68, 'App\\Models\\User', 5, 'authToken', 'd4ed53359af95ee50549b39f478f6a32e2b6c4aa672fad83caa701be927cab74', '["*"]', NULL, '2024-01-15 07:39:12', '2024-01-15 07:39:12'),
	(69, 'App\\Models\\User', 5, 'authToken', '6cbf34ad7d6cb408b9d398bededa4c62b1b519b10adeaa2243551d94c6e4d21b', '["*"]', NULL, '2024-01-15 07:41:01', '2024-01-15 07:41:01'),
	(70, 'App\\Models\\User', 5, 'authToken', '6037d0dc57f68302097d301e88cb8fb2c70e24f029818e5e7e5411d03bdc9b6b', '["*"]', NULL, '2024-01-15 07:41:51', '2024-01-15 07:41:51'),
	(71, 'App\\Models\\User', 5, 'authToken', 'd1f83dc71374949b431aab60fd91af4f6e3ddccae7dd7e53abb43ed6c7cbc4a9', '["*"]', NULL, '2024-01-15 07:48:32', '2024-01-15 07:48:32'),
	(72, 'App\\Models\\User', 5, 'authToken', '1f9481a1e2198ceb07d6569b3f5dc77dbdcd5ccf2851940fefbbee30a99aa0c9', '["*"]', NULL, '2024-01-15 07:49:14', '2024-01-15 07:49:14'),
	(73, 'App\\Models\\User', 5, 'authToken', '587e9b38fa5b8f1b8223d0f905a28373d1e81ef2efeb7285f3e9c0eb2d339431', '["*"]', NULL, '2024-01-15 07:50:00', '2024-01-15 07:50:00'),
	(74, 'App\\Models\\User', 5, 'authToken', 'b19e562f38043cd93178a83cc3d5dd78c79aa29ba00233509d47b0305618fddf', '["*"]', NULL, '2024-01-15 07:51:20', '2024-01-15 07:51:20'),
	(75, 'App\\Models\\User', 5, 'authToken', 'a94885ef6852f27d0242dcb6aa728ca31ab5f1f83b4ded3efdf5f67bbaecb0bc', '["*"]', NULL, '2024-01-15 07:52:21', '2024-01-15 07:52:21'),
	(76, 'App\\Models\\User', 5, 'authToken', '3494ae8ee38b5bda0d4738e919c1b9f5409a7e60f256ffc310d44aaa01bb33fd', '["*"]', NULL, '2024-01-15 07:53:13', '2024-01-15 07:53:13'),
	(77, 'App\\Models\\User', 5, 'authToken', 'd871a7a8ba9ddf9d46cc76c24db2b8acf04441da1518bb3d9454f5941089ec1f', '["*"]', NULL, '2024-01-15 07:54:14', '2024-01-15 07:54:14'),
	(78, 'App\\Models\\User', 5, 'authToken', 'abfdaf4b7e58679d0afbb8ad87e40e381f3f2e22ce1dd638f1f49ecbee1a5d69', '["*"]', NULL, '2024-01-15 08:07:10', '2024-01-15 08:07:10'),
	(79, 'App\\Models\\User', 5, 'authToken', 'c63eea25c51010db0eafa0ffac53775cd2d5f3f239acfab799b1587cdc0a7fc2', '["*"]', NULL, '2024-01-15 08:08:30', '2024-01-15 08:08:30'),
	(80, 'App\\Models\\User', 5, 'authToken', 'b6246b771b2d013496b513863c3a80894c9024e3d38d3b447acd78a78891d062', '["*"]', NULL, '2024-01-15 08:40:14', '2024-01-15 08:40:14'),
	(81, 'App\\Models\\User', 5, 'authToken', 'b60201d15fd431b33111aacd0246faafd10a90d2c02b4ecfdf444d32e365b115', '["*"]', NULL, '2024-01-15 08:43:11', '2024-01-15 08:43:11'),
	(82, 'App\\Models\\User', 5, 'authToken', '542f735fc0040a2369fee7f65a2a0056b19267172681146d1f379abd5077f0fb', '["*"]', NULL, '2024-01-15 08:53:57', '2024-01-15 08:53:57'),
	(83, 'App\\Models\\User', 7, 'authToken', '1ac7e890a8fdf8ff5dba71fd3dbf193db219cf5f3a3167d69932e5d3fda3c73e', '["*"]', '2024-01-15 14:08:44', '2024-01-15 14:08:43', '2024-01-15 14:08:44'),
	(84, 'App\\Models\\User', 5, 'authToken', 'f2d8c5d90f2fc34e951e6726b4949675ee334dab38409b35913bafdcbd8f6e69', '["*"]', NULL, '2024-01-15 14:09:13', '2024-01-15 14:09:13'),
	(85, 'App\\Models\\User', 8, 'authToken', '716eed9afeac1c95366c19781b1a86c9ccd16b94c97be8fe4c51f1b2931330c0', '["*"]', '2024-01-15 14:10:56', '2024-01-15 14:10:55', '2024-01-15 14:10:56'),
	(86, 'App\\Models\\User', 8, 'authToken', 'b3981cb875ac4d4b30a0b6347d6db22958c72448e60c9ad23fa4a87992b48661', '["*"]', NULL, '2024-01-15 14:12:16', '2024-01-15 14:12:16'),
	(87, 'App\\Models\\User', 7, 'authToken', 'fff0d80ab060330290694021e1ced51aa58a9e9e982cae7a111cf4517a410ebe', '["*"]', NULL, '2024-01-15 14:12:21', '2024-01-15 14:12:21'),
	(88, 'App\\Models\\User', 7, 'authToken', '6651a93d663842cc5c38f8ee372b70d8ea6cf120ed8ff30794cae0d220f777dc', '["*"]', NULL, '2024-01-15 14:16:06', '2024-01-15 14:16:06'),
	(89, 'App\\Models\\User', 7, 'authToken', 'f5520d44cfd95cc30fa816a44765467c95e03308b5cebebfa39b971c737d7935', '["*"]', NULL, '2024-01-15 14:17:48', '2024-01-15 14:17:48'),
	(90, 'App\\Models\\User', 7, 'authToken', '960fd29cfe7004532dc352155a1a5897c7289e6186b8d0af2bf7cdb3de2743e1', '["*"]', '2024-01-15 14:48:33', '2024-01-15 14:47:54', '2024-01-15 14:48:33'),
	(91, 'App\\Models\\User', 9, 'authToken', '89eeb0b0414e7a4353999f5f969329f1d535e295041e9a4417b8ac427cf66ce0', '["*"]', '2024-01-15 15:40:46', '2024-01-15 15:40:46', '2024-01-15 15:40:46'),
	(92, 'App\\Models\\User', 9, 'authToken', 'e7cd57a3f269a58add7689820c4e9cf6668d675c5c4ca2994dbee2a78fbf2955', '["*"]', NULL, '2024-01-15 15:41:35', '2024-01-15 15:41:35'),
	(93, 'App\\Models\\User', 9, 'authToken', '12f4169402d36fd7026da88ecae81597baab881ccc7473b393c069727e7b9fe4', '["*"]', NULL, '2024-01-15 16:30:46', '2024-01-15 16:30:46'),
	(94, 'App\\Models\\User', 9, 'authToken', '6a1bfa5d6d9a4c9abb3947ae1e36e2e01241e0417f78ef35a5149c9ad307e4ec', '["*"]', NULL, '2024-01-16 02:50:45', '2024-01-16 02:50:45'),
	(95, 'App\\Models\\User', 9, 'authToken', 'd90b365757703afbb768b4802f58df62a87d940a53ecc76a68da3024b1bdd247', '["*"]', NULL, '2024-01-16 04:13:46', '2024-01-16 04:13:46'),
	(96, 'App\\Models\\User', 9, 'authToken', '25c7dab3fbe06542afe654f2181140a6448410a1119159c7ea0055ea9db69ee8', '["*"]', NULL, '2024-01-16 06:34:39', '2024-01-16 06:34:39'),
	(97, 'App\\Models\\User', 9, 'authToken', 'ba91ac70ed645b34cd67776911db46e01a54c6948d059d50ad002f948d4621fa', '["*"]', NULL, '2024-01-16 06:47:16', '2024-01-16 06:47:16'),
	(98, 'App\\Models\\User', 9, 'authToken', '52a0358e72efd1fea0da7c4f037c54f45949750e6c3c53547869a519bf3e0067', '["*"]', NULL, '2024-01-16 15:42:41', '2024-01-16 15:42:41'),
	(99, 'App\\Models\\User', 9, 'authToken', '9769c54f0e4da3c6ac974f88187d2897d589b5ea63c148ecdab23967fc240bca', '["*"]', NULL, '2024-01-19 03:00:58', '2024-01-19 03:00:58'),
	(100, 'App\\Models\\User', 9, 'authToken', '76a31012daedbb5b1b128e729446129082e782af5b09cd3560385d7b73d862ed', '["*"]', NULL, '2024-01-20 05:50:08', '2024-01-20 05:50:08'),
	(101, 'App\\Models\\User', 5, 'authToken', '94bf5b913b9ce7d57efce8e5ab391c548cc7418e6faa34f652ab973d8f77bced', '["*"]', NULL, '2024-01-20 06:57:05', '2024-01-20 06:57:05'),
	(102, 'App\\Models\\User', 5, 'authToken', '47df1bd70d9576e63c60c0798fc3faf59823b3a9dce6592b4d4b4e97a6f9c55d', '["*"]', NULL, '2024-01-20 07:02:27', '2024-01-20 07:02:27'),
	(103, 'App\\Models\\User', 5, 'authToken', 'bfac17bfdef68af0ec44801ac7418d7f73e602e15e9f267da27856b527afdea7', '["*"]', NULL, '2024-01-20 07:08:15', '2024-01-20 07:08:15'),
	(104, 'App\\Models\\User', 5, 'authToken', 'd583e6f932ff4c871461609af0d59e6688136bc58b3f5dbccde0ac28629bab97', '["*"]', NULL, '2024-01-20 07:39:41', '2024-01-20 07:39:41'),
	(105, 'App\\Models\\User', 5, 'authToken', '5a8006f5fbfc0a026430760e166e3c2cc678e738b06403ae65054df60a5b7241', '["*"]', NULL, '2024-01-20 08:23:56', '2024-01-20 08:23:56'),
	(106, 'App\\Models\\User', 5, 'authToken', '4a37e3a2a4d9543873c27cb51deec74e17ea14d7f1c029536ce594e2280af8c8', '["*"]', NULL, '2024-01-20 09:16:08', '2024-01-20 09:16:08'),
	(107, 'App\\Models\\User', 5, 'authToken', '7543859e7c115d3dab661012c66dbe503389bf9cafdcd7a2c4aa150171aaa774', '["*"]', NULL, '2024-01-20 09:22:16', '2024-01-20 09:22:16'),
	(108, 'App\\Models\\User', 5, 'authToken', '10daa0bc3dd7a74fcc5df6c599d58e67d3238b78e2da4b3e4acaef27f1b774ff', '["*"]', NULL, '2024-01-20 09:27:24', '2024-01-20 09:27:24'),
	(109, 'App\\Models\\User', 5, 'authToken', 'ada5c60944a83551b9841b46748ce0c5bc6aa72f74d7f01c3e86aba973d1c848', '["*"]', NULL, '2024-01-20 09:28:52', '2024-01-20 09:28:52'),
	(110, 'App\\Models\\User', 5, 'authToken', '71938810e7cc23991a2e944c27dfa9f29aab9857d35fac93f33236e01562fa16', '["*"]', NULL, '2024-01-20 09:52:33', '2024-01-20 09:52:33'),
	(111, 'App\\Models\\User', 5, 'authToken', 'd280249c66dc1341631f7d8b3c3783e97c70c949c247905326bdc0ed68526107', '["*"]', NULL, '2024-01-22 13:47:03', '2024-01-22 13:47:03'),
	(112, 'App\\Models\\User', 5, 'authToken', '928efa3a473bcfe304bf40ee6f09a59ab9379bdefa1fbf69cc649d359d60ac93', '["*"]', NULL, '2024-01-22 13:50:12', '2024-01-22 13:50:12'),
	(113, 'App\\Models\\User', 5, 'authToken', 'be5a1e6e01aeca65a7b2250fcfd376bc8ad16a9bd41b1f63d8ad987f36fa118e', '["*"]', NULL, '2024-01-22 13:59:03', '2024-01-22 13:59:03'),
	(114, 'App\\Models\\User', 5, 'authToken', '2517ed2f394ea3f68c51a2d45058c58607385c9b9fd0a0e397dbf102404690ed', '["*"]', NULL, '2024-01-22 14:03:53', '2024-01-22 14:03:53'),
	(115, 'App\\Models\\User', 5, 'authToken', 'cbd39af626ca1fc0595b9c886547953b8f122274fc8afab66ce11e1c2e89569a', '["*"]', NULL, '2024-01-22 14:04:41', '2024-01-22 14:04:41'),
	(116, 'App\\Models\\User', 5, 'authToken', 'd175ea01bc764fc9e53a7d9e939a5cad5455ed08ad398c492d96062c02a99099', '["*"]', NULL, '2024-01-22 14:05:14', '2024-01-22 14:05:14'),
	(117, 'App\\Models\\User', 5, 'authToken', 'd00249c09bec732dd9815f8d4b9b2cd44afd59b686a1d21e2962ad7f13096909', '["*"]', NULL, '2024-01-22 14:07:28', '2024-01-22 14:07:28'),
	(118, 'App\\Models\\User', 5, 'authToken', 'a6c820a16ce09fc81e8140c96cdd29eae68c4687062f1f10572023d9dd0aee22', '["*"]', NULL, '2024-01-22 14:09:48', '2024-01-22 14:09:48'),
	(119, 'App\\Models\\User', 5, 'authToken', 'e78b9c106044054fbaf83336df8386feea4171d0466648bda9e19b558201579a', '["*"]', NULL, '2024-01-22 14:10:51', '2024-01-22 14:10:51'),
	(120, 'App\\Models\\User', 5, 'authToken', '78a8e9a2750bc03d4e67aa881e57a8a6d312567cf7f88b09fe84c1c701cea518', '["*"]', NULL, '2024-01-22 14:19:37', '2024-01-22 14:19:37'),
	(121, 'App\\Models\\User', 5, 'authToken', '7877068d42a37707b7241689fccb71140ef31f7404efeb024cfa16d9d2f7cc8e', '["*"]', NULL, '2024-01-22 14:38:55', '2024-01-22 14:38:55'),
	(122, 'App\\Models\\User', 5, 'authToken', '282b50d8735a360fc7de56f54680e7e25055fcfbd74c05499fe166503c9c3fce', '["*"]', NULL, '2024-01-22 15:14:25', '2024-01-22 15:14:25'),
	(123, 'App\\Models\\User', 9, 'authToken', '8b9143be9c4b5fd443255309ed3bd60f70daa3398a6a8bc66c5b9d6e5e38449c', '["*"]', NULL, '2024-01-22 15:45:50', '2024-01-22 15:45:50'),
	(124, 'App\\Models\\User', 9, 'authToken', '9be61adade8df80ca76ee9eaeba7fe70cbe996b6795a15f36ec5552ad24160df', '["*"]', NULL, '2024-01-22 15:50:17', '2024-01-22 15:50:17'),
	(125, 'App\\Models\\User', 9, 'authToken', '952e0ba997283f43674bde6ad60bd5b56caf6fabe9809419d82260036b45bb7a', '["*"]', NULL, '2024-01-22 15:51:42', '2024-01-22 15:51:42'),
	(126, 'App\\Models\\User', 9, 'authToken', '807a40fe5a92ed29893e8a99d882cf3755e8de5418728ade583d97d203f992cf', '["*"]', NULL, '2024-01-22 15:53:32', '2024-01-22 15:53:32'),
	(127, 'App\\Models\\User', 5, 'authToken', '1842294e96ac67c7cd18a9e8cdce704b7aab5da3a6ee84d13d401a6407bbf198', '["*"]', NULL, '2024-01-22 16:27:41', '2024-01-22 16:27:41'),
	(128, 'App\\Models\\User', 1, 'authToken', 'bfac9430192123202d917459e242d939d634d81565443bd5880f70e12f94569b', '["*"]', NULL, '2024-01-22 16:54:20', '2024-01-22 16:54:20'),
	(129, 'App\\Models\\User', 10, 'authToken', '212a28274f41ef901c15c17b500a7a6a56bc49cf06c823fcc0e6ee45586d9dc8', '["*"]', '2024-01-22 17:04:06', '2024-01-22 17:04:05', '2024-01-22 17:04:06'),
	(130, 'App\\Models\\User', 10, 'authToken', 'daa62e66b9c1a555a563d7d114d715d826d48c30c56ae06efdb0fbe548362372', '["*"]', NULL, '2024-01-22 17:07:04', '2024-01-22 17:07:04'),
	(131, 'App\\Models\\User', 5, 'authToken', '1797892d862e1a5134002a68cb7c2adb86ead787eee3172970ea8089f307221d', '["*"]', NULL, '2024-01-22 17:08:07', '2024-01-22 17:08:07'),
	(132, 'App\\Models\\User', 10, 'authToken', '5e6a295dde9e4ed586fb8d29374d42fb7d796d99d90d57b825b420e8523b1770', '["*"]', NULL, '2024-01-22 17:09:29', '2024-01-22 17:09:29'),
	(133, 'App\\Models\\User', 10, 'authToken', '917c14befaff3cdc8192bb5d008f330d40569a3fb2dc2da6d933cfee3f0be6c4', '["*"]', NULL, '2024-01-22 17:11:39', '2024-01-22 17:11:39'),
	(134, 'App\\Models\\User', 5, 'authToken', '79036c4678d96c9994929a23d0546c7a6acaea3d6e3c4c70255a2932902958b7', '["*"]', NULL, '2024-01-22 17:12:36', '2024-01-22 17:12:36'),
	(135, 'App\\Models\\User', 11, 'authToken', '8e2f73ecb1cb5618ff54e4cdbb959951183ea116c89682f0395944b44cb9f444', '["*"]', '2024-01-22 17:15:20', '2024-01-22 17:15:20', '2024-01-22 17:15:20'),
	(136, 'App\\Models\\User', 11, 'authToken', '6f27ed8d1301c1d158bd2955df34565077974808eab233e0bd0ee3ebe916f57a', '["*"]', NULL, '2024-01-22 17:17:18', '2024-01-22 17:17:18'),
	(137, 'App\\Models\\User', 11, 'authToken', '2bc1f615b288f4e55bdea91f0abd21e96dd3bec1ee6dae2d28952342cc7e7906', '["*"]', NULL, '2024-01-22 17:18:13', '2024-01-22 17:18:13'),
	(138, 'App\\Models\\User', 11, 'authToken', '8dbc2aa4becd6b6e406f4ef1f15fef67eedaf22b3737319f79d0968fe6c7cf75', '["*"]', NULL, '2024-01-22 17:19:25', '2024-01-22 17:19:25'),
	(139, 'App\\Models\\User', 5, 'authToken', '9d848ed1d13ff56238b65b445896cab0f1f260e0b60d8eccc7e853d9c4794b70', '["*"]', NULL, '2024-01-22 18:00:20', '2024-01-22 18:00:20'),
	(140, 'App\\Models\\User', 5, 'authToken', '6f1a244186cc8669102ae574e8e04873d71160b6e442e727031a4baa311f22b4', '["*"]', NULL, '2024-01-22 18:00:20', '2024-01-22 18:00:20'),
	(141, 'App\\Models\\User', 11, 'authToken', '445e5f09d387eaa5837d5ae20e147b9468a137513596dc7671ee29aecdc97871', '["*"]', NULL, '2024-01-22 18:03:03', '2024-01-22 18:03:03'),
	(142, 'App\\Models\\User', 5, 'authToken', '2ada96b41c529902143d309b3128f9287dfec5987dbe3f85417e478eb1dd96f0', '["*"]', NULL, '2024-01-22 18:03:38', '2024-01-22 18:03:38'),
	(143, 'App\\Models\\User', 9, 'authToken', '23b3d75e99b85cb9522e86c680d921a30af6964cbfd598f3e8739e3c7b103cbe', '["*"]', NULL, '2024-01-22 18:06:54', '2024-01-22 18:06:54'),
	(144, 'App\\Models\\User', 5, 'authToken', '74b1c774ccb7eeddac93e95d3640a37b3d77f1b74214c3faf277a1bc77cd13a5', '["*"]', NULL, '2024-01-22 18:14:40', '2024-01-22 18:14:40'),
	(145, 'App\\Models\\User', 5, 'authToken', '870abd9a2e5161f2ea4934bf0481d736ce9ebdddf152f0d9f44e6c53b8c65d31', '["*"]', NULL, '2024-01-22 18:23:52', '2024-01-22 18:23:52'),
	(146, 'App\\Models\\User', 5, 'authToken', '8f0fcf4dfb07d91e9f8f154311f9b67aed30715c96006f3110b6740476491027', '["*"]', NULL, '2024-01-22 18:26:20', '2024-01-22 18:26:20'),
	(147, 'App\\Models\\User', 5, 'authToken', '520fd534215e7b0d7574a46655c0d70a1f1f4ad6c2464ca939f497da3aead171', '["*"]', NULL, '2024-01-22 18:28:05', '2024-01-22 18:28:05'),
	(148, 'App\\Models\\User', 5, 'authToken', '73d7db432be944696850af59cd0a6d7b779dc491416dac5ff4f3e44fbb941443', '["*"]', NULL, '2024-01-22 18:29:10', '2024-01-22 18:29:10'),
	(149, 'App\\Models\\User', 5, 'authToken', '205ed20f84112c9a91c9e3333aedc7bc38c2c758f76de61e81d5b85e9bbdae60', '["*"]', NULL, '2024-01-22 18:29:52', '2024-01-22 18:29:52'),
	(150, 'App\\Models\\User', 12, 'authToken', '083b95d893fba07a31d66c4687502e7807a0e62ce81176bcbf5785edf7bfa79a', '["*"]', NULL, '2024-01-22 19:51:18', '2024-01-22 19:51:18'),
	(151, 'App\\Models\\User', 12, 'authToken', '58c05183f6bb2daa6214a67d95d05e67fd92f0e4ab6369a836291de7c300d344', '["*"]', NULL, '2024-01-22 19:56:43', '2024-01-22 19:56:43'),
	(152, 'App\\Models\\User', 12, 'authToken', 'cecb613751a636e33b422a6954a47ccdf37ec80cb12584c753144dd9f5153c98', '["*"]', NULL, '2024-01-22 22:04:44', '2024-01-22 22:04:44'),
	(153, 'App\\Models\\User', 5, 'authToken', '196cfb2e5edd59a9218f40ebb8488a9634329142e848a299889781b1169bf53c', '["*"]', NULL, '2024-01-23 00:15:26', '2024-01-23 00:15:26'),
	(154, 'App\\Models\\User', 5, 'authToken', '197b2d783ddb1e601a5ff97844b8d5d4a8b0b87179a45bf4dc1d51646b3bca3b', '["*"]', NULL, '2024-01-23 00:17:25', '2024-01-23 00:17:25'),
	(155, 'App\\Models\\User', 5, 'authToken', '29e56efe07be518d29f34f447278da14487e677b8216fa019ba6b184f5853b1e', '["*"]', NULL, '2024-01-23 00:22:46', '2024-01-23 00:22:46'),
	(157, 'App\\Models\\User', 5, 'authToken', '86bd94b844d0ac79dfd27bc8e325c3353ecbe1aa99dcf4eec305ad2d504f41bb', '["*"]', '2024-01-23 00:28:35', '2024-01-23 00:28:32', '2024-01-23 00:28:35'),
	(162, 'App\\Models\\User', 5, 'authToken', '9c615fd50ae452e42fdbb620ab808436f4b5e01ed008696da35142dd469a4246', '["*"]', NULL, '2024-01-23 00:42:59', '2024-01-23 00:42:59'),
	(163, 'App\\Models\\User', 5, 'authToken', 'd925a1a8df6b9e534e9cbb3030a79f7e94f6d3471ad51b14c84e24d1cc22bce2', '["*"]', NULL, '2024-01-23 00:44:08', '2024-01-23 00:44:08'),
	(166, 'App\\Models\\User', 11, 'authToken', '49766b6fe8aa615258327c1e150d849668b91770d6bca6a376ad3f055b7d8eea', '["*"]', NULL, '2024-01-23 01:02:57', '2024-01-23 01:02:57'),
	(167, 'App\\Models\\User', 9, 'authToken', 'f8d57d6abaaba516c9721df9e3d9f047677522597f00f8812f848041184609e7', '["*"]', NULL, '2024-01-23 01:09:57', '2024-01-23 01:09:57'),
	(168, 'App\\Models\\User', 9, 'authToken', '3e0e151e1b092ba62a81826173115f2a7496300a5e35d8196f2ff0e29b162251', '["*"]', NULL, '2024-01-23 02:20:17', '2024-01-23 02:20:17'),
	(169, 'App\\Models\\User', 12, 'authToken', 'a288974c8a36c658cac9dd4e63cbd077589f5a778d1f4b31d0d59c845cec3b70', '["*"]', NULL, '2024-01-23 02:20:28', '2024-01-23 02:20:28'),
	(170, 'App\\Models\\User', 12, 'authToken', 'c0899afce754fe877dead32ff115a11e0f9725ddf8c5a0e48327fef51a1ca023', '["*"]', NULL, '2024-01-23 02:21:01', '2024-01-23 02:21:01'),
	(171, 'App\\Models\\User', 11, 'authToken', 'e21a61872dea8fb944eb0b5d793090ed8c2e91e12e5fc46d7e79077f5edd55b7', '["*"]', NULL, '2024-01-23 02:21:14', '2024-01-23 02:21:14'),
	(172, 'App\\Models\\User', 9, 'authToken', 'fbe62b9335565b716b6cc073cd693533367a882f80243da34e566f50d95acfb5', '["*"]', NULL, '2024-01-23 02:46:09', '2024-01-23 02:46:09'),
	(174, 'App\\Models\\User', 5, 'authToken', 'b6fe9daa97b7fbb677c40bc9fc5b48ec253c5e3998f976dba2c75dcbbc7642db', '["*"]', NULL, '2024-01-23 03:08:44', '2024-01-23 03:08:44'),
	(175, 'App\\Models\\User', 5, 'authToken', 'f306cabfb6c74c45af250ca2e32f8a42432c4170bfd7244ba8722371388fd846', '["*"]', NULL, '2024-01-23 03:09:40', '2024-01-23 03:09:40'),
	(176, 'App\\Models\\User', 5, 'authToken', 'f4223ef52781c816a0750801aa6892bc1ade366894c24446083198d500b71678', '["*"]', NULL, '2024-01-23 03:10:34', '2024-01-23 03:10:34'),
	(177, 'App\\Models\\User', 5, 'authToken', '34a1f58cf2bffebc9af1b1fd33aa1337b19a75aee937f0f0fdf521fd8200e041', '["*"]', NULL, '2024-01-23 03:16:57', '2024-01-23 03:16:57'),
	(178, 'App\\Models\\User', 5, 'authToken', 'd96a8dd9ede9d32b138aebdd06588eb65656a82c1fbf69104146f84a1ddaac1d', '["*"]', NULL, '2024-01-23 03:30:44', '2024-01-23 03:30:44'),
	(179, 'App\\Models\\User', 5, 'authToken', '5daac344ee713fc47a8d1cc66c274c14745896013aae4f6b561c4be5ba089984', '["*"]', NULL, '2024-01-23 03:31:51', '2024-01-23 03:31:51'),
	(180, 'App\\Models\\User', 10, 'authToken', '0e7acb41aa203fff259cf7e22d86b8f0adf9815f1a45c876ae665153956ce23e', '["*"]', NULL, '2024-01-23 03:33:05', '2024-01-23 03:33:05'),
	(181, 'App\\Models\\User', 5, 'authToken', '42c3172dd4ac3079d22da5ba2bf3656a68af36571195f1d051ddd2383aad4daf', '["*"]', NULL, '2024-01-23 03:40:08', '2024-01-23 03:40:08'),
	(182, 'App\\Models\\User', 5, 'authToken', '48d916716e48c781fda75a39ba6e3ebcb7a1bbc388a0f1c6d1370802c3e9450b', '["*"]', NULL, '2024-01-23 03:44:12', '2024-01-23 03:44:12'),
	(183, 'App\\Models\\User', 5, 'authToken', '64cd0fe49fcf413d37f75a0236b77e6199c5df8fd80cebd07e142e157d2d54ce', '["*"]', NULL, '2024-01-23 03:53:43', '2024-01-23 03:53:43'),
	(184, 'App\\Models\\User', 5, 'authToken', 'd78ad85cdcbb00484795322de40094ba8e2e8c85c312b3d6f68209f4cf5ae854', '["*"]', NULL, '2024-01-23 03:55:08', '2024-01-23 03:55:08'),
	(185, 'App\\Models\\User', 5, 'authToken', '94f056e652812abfd0c9faf272dd1e84e9cc2f4007f15db6e53d3067a1582ea8', '["*"]', NULL, '2024-01-23 03:56:01', '2024-01-23 03:56:01'),
	(186, 'App\\Models\\User', 5, 'authToken', 'ec2f4888c45b3fcc44dac3fce95f6842dda31309937a12266c6a5dae1ee3dbaa', '["*"]', NULL, '2024-01-23 03:58:25', '2024-01-23 03:58:25'),
	(187, 'App\\Models\\User', 9, 'authToken', '008a946421eb927c073a88a879ecbbb3ed031869cd0c45718d38598bcc385d9d', '["*"]', NULL, '2024-01-23 04:27:29', '2024-01-23 04:27:29'),
	(191, 'App\\Models\\User', 9, 'authToken', '2adffe85151664a688b246d73007b58c045c04a02d1093ca66d7d17c46f4f832', '["*"]', NULL, '2024-01-23 05:22:27', '2024-01-23 05:22:27'),
	(193, 'App\\Models\\User', 12, 'authToken', 'c5e0dd8e9877b7e039fe09890c338c42c99c6ae15b208546bca0cd3c64e53953', '["*"]', NULL, '2024-01-23 07:53:34', '2024-01-23 07:53:34'),
	(194, 'App\\Models\\User', 12, 'authToken', '4b26653e735ff3b73312da98fa73bd14a18a4c9057e1a65607b07aa7662a50e2', '["*"]', NULL, '2024-01-23 07:55:43', '2024-01-23 07:55:43'),
	(195, 'App\\Models\\User', 12, 'authToken', '065229209732cb4f9a49c14f2273b43cec5e5f837c9219869a9cf015258289a0', '["*"]', NULL, '2024-01-23 08:07:13', '2024-01-23 08:07:13'),
	(197, 'App\\Models\\User', 12, 'authToken', '6ce6c3acad1645a25ef30193a8eee9797df475de67f3aa695da3d0e872e9d00e', '["*"]', NULL, '2024-01-23 10:05:02', '2024-01-23 10:05:02'),
	(198, 'App\\Models\\User', 12, 'authToken', 'ce0b768025046517e98c49a785339e300251a3fa1ccabb360d0dd5ca7e5d8962', '["*"]', NULL, '2024-01-23 12:00:37', '2024-01-23 12:00:37'),
	(200, 'App\\Models\\User', 9, 'authToken', '1c07930bb55a0bb5d584007e8bf7eb12b131650a2fb33ee7aaea1538a4ede3e7', '["*"]', NULL, '2024-01-23 13:55:52', '2024-01-23 13:55:52'),
	(202, 'App\\Models\\User', 5, 'authToken', 'ca304d298e12e7b34d2e24a29c7e3816cad2b68778a7ff55c8dcf15828e3a353', '["*"]', NULL, '2024-01-25 09:14:47', '2024-01-25 09:14:47'),
	(203, 'App\\Models\\User', 5, 'authToken', '8d1665a1db2dec8f8215da17cea8f1648a215ccc643ae8eaae051b21266fde78', '["*"]', NULL, '2024-01-25 09:18:49', '2024-01-25 09:18:49'),
	(204, 'App\\Models\\User', 5, 'authToken', '3a2e38e42cfb0abbc9679f289a39905049ff0b1e57d5125f143214de24e4a903', '["*"]', NULL, '2024-01-25 09:57:31', '2024-01-25 09:57:31'),
	(205, 'App\\Models\\User', 5, 'authToken', '8cdadb727a418de05bf17959d463f57b9fb82ab0bdf0eb4bce854d6344c609d7', '["*"]', NULL, '2024-01-25 10:14:17', '2024-01-25 10:14:17'),
	(206, 'App\\Models\\User', 5, 'authToken', '8da2679bb45b8734a82af71ea7aa7223d6a3e40246d159dec0951d3e1c98371e', '["*"]', NULL, '2024-01-25 16:03:52', '2024-01-25 16:03:52'),
	(207, 'App\\Models\\User', 5, 'authToken', '1ce67b717744f44f57b2d16826ea7bae59d09a2abedf1c49ff0be8369d41caa2', '["*"]', NULL, '2024-01-25 16:11:11', '2024-01-25 16:11:11'),
	(208, 'App\\Models\\User', 5, 'authToken', '289777a994ef5dc4be78d5121052d328040a3e47725643eb310e139734336a9e', '["*"]', NULL, '2024-01-25 16:12:36', '2024-01-25 16:12:36'),
	(209, 'App\\Models\\User', 13, 'authToken', '26761ded201aca50fef4499b822bdc2217768248de7678b50bb8266b9c09c9f7', '["*"]', '2024-01-26 08:34:37', '2024-01-26 08:34:34', '2024-01-26 08:34:37'),
	(210, 'App\\Models\\User', 13, 'authToken', 'fb9210d7ded757783a765bf156c2169b7c365c7e7826baf7fa1b0d969129241e', '["*"]', '2024-01-26 08:37:01', '2024-01-26 08:36:32', '2024-01-26 08:37:01'),
	(213, 'App\\Models\\User', 9, 'authToken', '60693be49ece6cffeb40a6bdb1a902871900a51825b891c94b3de8da44d9b3c3', '["*"]', NULL, '2024-01-27 10:29:02', '2024-01-27 10:29:02'),
	(214, 'App\\Models\\User', 13, 'authToken', '17b7ca2d52c9002486f2dfa0a64d9f6326eb4a92003906f08b5298aff7937182', '["*"]', NULL, '2024-01-27 16:40:45', '2024-01-27 16:40:45'),
	(216, 'App\\Models\\User', 13, 'authToken', '375d3af5b678bb81291ff452bdda4dac931e066ab6b98af861f07e2f0f3d0dbd', '["*"]', NULL, '2024-01-30 22:08:10', '2024-01-30 22:08:10'),
	(217, 'App\\Models\\User', 9, 'authToken', '71318ff7fdfd8f391a50db1dee0bf2ded27a240caf1995d72a0913aaeed40937', '["*"]', NULL, '2024-01-31 02:05:57', '2024-01-31 02:05:57'),
	(218, 'App\\Models\\User', 9, 'authToken', '54bc52f01d65c32d2c584aa84354a2e5b6dee680241f80f284dbab3b1141e5e9', '["*"]', NULL, '2024-01-31 02:43:30', '2024-01-31 02:43:30'),
	(219, 'App\\Models\\User', 9, 'authToken', '8aa307a6767cfd13a2c68f690427ae1efeb8243672b701d33319b1069c51fbf5', '["*"]', NULL, '2024-01-31 06:11:44', '2024-01-31 06:11:44'),
	(220, 'App\\Models\\User', 14, 'authToken', 'd60059a3343541e8296ae4281a9698445f87c6e226c128fbb68189cd0ace9291', '["*"]', NULL, '2024-01-31 06:31:52', '2024-01-31 06:31:52'),
	(221, 'App\\Models\\User', 5, 'authToken', '54c9c29e19438799c407999a1363d33612ea85e169d17b15c37592fe710df5e1', '["*"]', NULL, '2024-02-01 15:00:49', '2024-02-01 15:00:49'),
	(222, 'App\\Models\\User', 5, 'authToken', 'd867de613a150305bcdef4bff0babc29e7bf45ee4166f8e42a2a1796fb89db7a', '["*"]', NULL, '2024-02-01 15:12:17', '2024-02-01 15:12:17'),
	(223, 'App\\Models\\User', 5, 'authToken', 'a97be6741c6d7e04790c9a1a3a347b0bdd1f632f9d71dda76d78d90058f15213', '["*"]', NULL, '2024-02-01 15:15:43', '2024-02-01 15:15:43'),
	(224, 'App\\Models\\User', 5, 'authToken', 'ab75bbe073a2791cbc660f992df9179cef3d3336fbff9c332e648600a274782d', '["*"]', NULL, '2024-02-01 15:26:19', '2024-02-01 15:26:19'),
	(225, 'App\\Models\\User', 5, 'authToken', 'ac5d1a94830b2867901806c1e751054a93ac9e5cc26f96ba7049801b78b22ee8', '["*"]', NULL, '2024-02-01 15:27:14', '2024-02-01 15:27:14'),
	(226, 'App\\Models\\User', 9, 'authToken', '3282cd6cd6245486fb7e9a7d77744c66566fb58870174175c511268c1b2c0d31', '["*"]', NULL, '2024-02-01 16:02:09', '2024-02-01 16:02:09'),
	(227, 'App\\Models\\User', 9, 'authToken', '7d58e351764d71fade17bb6a10e3ceed8e997be9b668f529b4972ccdf317f0f4', '["*"]', NULL, '2024-02-01 16:04:13', '2024-02-01 16:04:13'),
	(228, 'App\\Models\\User', 9, 'authToken', 'bd88c112f47195ae167ebd39925a30fb1e5e51d49c6a7401ad237aeef46a78ca', '["*"]', NULL, '2024-02-01 16:06:41', '2024-02-01 16:06:41'),
	(229, 'App\\Models\\User', 9, 'authToken', '44cdc07e47e3bf6e34067ca19d43cce7c77401694ab41a717372ed79e2c339d1', '["*"]', NULL, '2024-02-01 16:07:16', '2024-02-01 16:07:16'),
	(230, 'App\\Models\\User', 9, 'authToken', 'e6f041773f98d3741a5ddd099b35db1ffe3cefb70cdc2e9049366178b4e8f3a9', '["*"]', NULL, '2024-02-01 16:09:07', '2024-02-01 16:09:07'),
	(231, 'App\\Models\\User', 9, 'authToken', '774dbc7a29624c05d2a6bc577235751cd7f08797b8e9d04b065746b98a8c5fa1', '["*"]', NULL, '2024-02-01 16:09:56', '2024-02-01 16:09:56'),
	(232, 'App\\Models\\User', 9, 'authToken', '148c7fc5180e9439cfa1c8d82a86a767192db0d9c3cf4a588126936cc24c3499', '["*"]', NULL, '2024-02-01 16:11:02', '2024-02-01 16:11:02'),
	(233, 'App\\Models\\User', 5, 'authToken', '3214b1c2970e70386de912451efb93c9101638792e2ebd86fc62abc2e0cacea3', '["*"]', NULL, '2024-02-01 16:32:03', '2024-02-01 16:32:03'),
	(234, 'App\\Models\\User', 5, 'authToken', '9769b45701aca93afab3601b682d30390bd64da1fe35ef76d5a79d36ce13659b', '["*"]', NULL, '2024-02-01 16:36:31', '2024-02-01 16:36:31'),
	(235, 'App\\Models\\User', 5, 'authToken', '2fa6ed21980fbb2680dc91124e764d679de801ebd561eb9e89cecdf10d3890a1', '["*"]', NULL, '2024-02-01 16:54:55', '2024-02-01 16:54:55'),
	(236, 'App\\Models\\User', 5, 'authToken', '66d7c3817dc0a06a3057f016e98fcdc369c6fdef78c6c06bf3b27d07c49807b5', '["*"]', NULL, '2024-02-01 17:06:10', '2024-02-01 17:06:10'),
	(238, 'App\\Models\\User', 9, 'authToken', '9a439e6d97000f82290f78e5e4b2d1f80e24efee4a955c011e90b91822873079', '["*"]', NULL, '2024-02-01 17:45:14', '2024-02-01 17:45:14'),
	(240, 'App\\Models\\User', 9, 'authToken', '7ade24d51f06bb4d830e9ffd40c83ccbaa8538ecea0dfb6adec32ab048ae62bc', '["*"]', NULL, '2024-02-06 05:11:36', '2024-02-06 05:11:36'),
	(241, 'App\\Models\\User', 9, 'authToken', '4f91b2244aab806c383fa723393d89fb719df9af2fc9a292c8f5c2bbf6f6686d', '["*"]', NULL, '2024-02-08 05:52:08', '2024-02-08 05:52:08'),
	(242, 'App\\Models\\User', 9, 'authToken', 'ce9311358ce0b878757b1de700405f107adab6ace0a205620ea316a7390a91d5', '["*"]', NULL, '2024-02-08 05:54:20', '2024-02-08 05:54:20'),
	(243, 'App\\Models\\User', 9, 'authToken', '06f93fdd3c1ab733ca53bc229a4dd3b50dc41f9c4ef3d73134dc94b04cd23d08', '["*"]', NULL, '2024-02-08 05:54:55', '2024-02-08 05:54:55'),
	(244, 'App\\Models\\User', 9, 'authToken', '53df2fb602d8d886e567c33939bb5c9290f65785c288bdc0e29e46f4a13241f3', '["*"]', NULL, '2024-02-08 05:58:00', '2024-02-08 05:58:00'),
	(245, 'App\\Models\\User', 9, 'authToken', '63a77f3513f8108a44d65dcfaca83d958a037f7f29c8f23276b3b55bfb12f33d', '["*"]', NULL, '2024-02-08 06:03:07', '2024-02-08 06:03:07'),
	(246, 'App\\Models\\User', 9, 'authToken', '9725ad1c450d34c4ce6b08c320f45b4b165c9055d5467e564d2ed42a16287e55', '["*"]', NULL, '2024-02-08 06:08:05', '2024-02-08 06:08:05'),
	(247, 'App\\Models\\User', 9, 'authToken', '13abcd4bc35d64e0b65ea4266fd201c43f7d2279498fb7263fb03474d310ef9f', '["*"]', NULL, '2024-02-08 11:09:10', '2024-02-08 11:09:10'),
	(248, 'App\\Models\\User', 9, 'authToken', '5eadd3f1b295f9e7c0efa599c8735a9bcbdd7bda52b6f0a7b6ad668339bec498', '["*"]', NULL, '2024-02-08 11:21:22', '2024-02-08 11:21:22'),
	(249, 'App\\Models\\User', 15, 'authToken', '060eb2bdb796fdcf53a3960f1150a9db11f90839f2ec8a57b8409c3fd8a36810', '["*"]', '2024-02-09 05:53:43', '2024-02-09 05:53:42', '2024-02-09 05:53:43'),
	(250, 'App\\Models\\User', 15, 'authToken', 'd2874a80ccacf93f9ff373cc3f6a5b7c79021a2e27778e8de3916c6c573090e1', '["*"]', NULL, '2024-02-09 05:55:17', '2024-02-09 05:55:17'),
	(251, 'App\\Models\\User', 15, 'authToken', 'b340d6fb3713cbc2d47b88bc2a205a128ad5da1ab8587c17ab2d5063ac3026af', '["*"]', NULL, '2024-02-09 05:56:13', '2024-02-09 05:56:13'),
	(252, 'App\\Models\\User', 15, 'authToken', 'ff4f0a36d0a3204a0483d977a5a8a1185740e69b85acea8084200407c05ecd6f', '["*"]', NULL, '2024-02-09 06:00:30', '2024-02-09 06:00:30'),
	(253, 'App\\Models\\User', 15, 'authToken', 'f38315c5cc85cd435d290be620abd639de5028d16f2c8da82c6f589c4bc4f59f', '["*"]', NULL, '2024-02-09 06:02:29', '2024-02-09 06:02:29'),
	(255, 'App\\Models\\User', 15, 'authToken', '58d6fe1de313092e94391b8bc9bcee363b677446f00547d4f1fe60addb803852', '["*"]', NULL, '2024-02-10 10:08:00', '2024-02-10 10:08:00'),
	(257, 'App\\Models\\User', 13, 'authToken', '7a3253d67ae7d8c7ffe8f79989b21c6b6f35418549cc8dbee6fe0d57c7d1417a', '["*"]', NULL, '2024-02-12 06:23:18', '2024-02-12 06:23:18'),
	(258, 'App\\Models\\User', 13, 'authToken', '6c07a062125ec8ab91ee9ed1c3daa173bce7b2d20428741dd6230944977f0403', '["*"]', NULL, '2024-02-12 06:48:15', '2024-02-12 06:48:15'),
	(259, 'App\\Models\\User', 16, 'authToken', '9ba37c10585ea0f6004e60439ec34b191ad77bd1ffdf8a0da65146a3c7d82835', '["*"]', NULL, '2024-02-12 06:50:36', '2024-02-12 06:50:36'),
	(260, 'App\\Models\\User', 9, 'authToken', 'c06e0da38dae2b05d67c8ba5ef68fe0c3b2f5cbe62be38e4e8af049350c6682f', '["*"]', NULL, '2024-02-12 12:48:50', '2024-02-12 12:48:50'),
	(261, 'App\\Models\\User', 9, 'authToken', 'b0db451db81d90712ae3ccb346b38dec29bf3550a6ddcf98dc9ce6d83098e500', '["*"]', NULL, '2024-02-12 12:53:19', '2024-02-12 12:53:19'),
	(262, 'App\\Models\\User', 9, 'authToken', '22467210cb67f3c0fc56644a3ec272b4013d349976f48c83db227e453bdb2f9c', '["*"]', NULL, '2024-02-12 13:19:41', '2024-02-12 13:19:41'),
	(263, 'App\\Models\\User', 9, 'authToken', '7dbf9b94b60caac5f0c57a28ac6b059014bf1a2a402b036ae05137ff93a6111a', '["*"]', NULL, '2024-02-12 13:23:30', '2024-02-12 13:23:30'),
	(264, 'App\\Models\\User', 9, 'authToken', '89db1598b531df18e1762d43459d3225044e95662be77e4d727ef00468e59ee7', '["*"]', NULL, '2024-02-12 13:24:41', '2024-02-12 13:24:41'),
	(265, 'App\\Models\\User', 9, 'authToken', '783eeea5c188345f8534293035cdd5c1802f72b965545a607c8645ad293cba0b', '["*"]', NULL, '2024-02-12 13:30:12', '2024-02-12 13:30:12'),
	(266, 'App\\Models\\User', 5, 'authToken', 'afc47baa4843eeb3be3f6cc7b88b5b7c4d22a1b79a254c0755b2ff5d8055e034', '["*"]', NULL, '2024-02-12 21:44:11', '2024-02-12 21:44:11'),
	(267, 'App\\Models\\User', 5, 'authToken', '9b1c057662ec505d3fcaf1161abf30184ab9a1f573ae993b952ca246095b4a6f', '["*"]', NULL, '2024-02-13 09:56:35', '2024-02-13 09:56:35'),
	(268, 'App\\Models\\User', 17, 'authToken', 'e09139477bc919aafb51594f89458410bcbfa15488a1b2bcec8d4b4a597f4a19', '["*"]', NULL, '2024-02-13 10:15:19', '2024-02-13 10:15:19'),
	(269, 'App\\Models\\User', 17, 'authToken', '9286bcaa8918fb3df26b1ac8db061e051fb99f3260ed32665542709421ed69f1', '["*"]', NULL, '2024-02-13 10:18:31', '2024-02-13 10:18:31'),
	(270, 'App\\Models\\User', 9, 'authToken', '9374051dd0f751fe4874b85e11029d5a3a37e83faa95490233df9bee293eb0f2', '["*"]', NULL, '2024-02-13 13:37:54', '2024-02-13 13:37:54'),
	(272, 'App\\Models\\User', 5, 'authToken', 'eb5b9e42332efe5ec4db3cb1952998a6be349f1ab33875a8d7ef58fe54f93694', '["*"]', NULL, '2024-02-13 16:52:47', '2024-02-13 16:52:47'),
	(273, 'App\\Models\\User', 5, 'authToken', 'ddbf944d090323b1c5323436370a9ac7ff77bf281cbf215667d093e6f0c25587', '["*"]', NULL, '2024-02-13 16:53:53', '2024-02-13 16:53:53'),
	(274, 'App\\Models\\User', 5, 'authToken', 'c55631a475afd1d29f09d52c6cff3cb3917985f8bf02701e515880b22291d6e9', '["*"]', NULL, '2024-02-13 17:03:21', '2024-02-13 17:03:21'),
	(276, 'App\\Models\\User', 5, 'authToken', 'b576334557cf373ef2c51f17e3ac365afc10a335b948bbedcb0e44f7cf4de5a3', '["*"]', NULL, '2024-02-13 17:06:54', '2024-02-13 17:06:54'),
	(277, 'App\\Models\\User', 5, 'authToken', '03bcb42b1ea7e96eddb98739d49e6290e2189bbbb4b0c222e94fb131290bef13', '["*"]', NULL, '2024-02-13 17:08:36', '2024-02-13 17:08:36'),
	(281, 'App\\Models\\User', 9, 'authToken', 'bc46d4e1357d85c2e9e3ae767989a59734faa514e0f29924a8eb1f9de3de3942', '["*"]', NULL, '2024-02-13 18:09:56', '2024-02-13 18:09:56'),
	(283, 'App\\Models\\User', 9, 'authToken', '2d8edf8292f3e5524f57eb038d7be6bc3d124b267316099e562b5a468191263d', '["*"]', NULL, '2024-02-13 22:25:13', '2024-02-13 22:25:13'),
	(284, 'App\\Models\\User', 9, 'authToken', '73f13e7bb828578198c0488658a4014299a176f58d60c0b3a9020862be27bf8f', '["*"]', NULL, '2024-02-13 22:29:29', '2024-02-13 22:29:29'),
	(285, 'App\\Models\\User', 5, 'authToken', '1d21adaacbf07f689731cd29ebe5a087ea2629fc1eaf3abc45c088fd8579a353', '["*"]', NULL, '2024-02-13 22:47:39', '2024-02-13 22:47:39'),
	(286, 'App\\Models\\User', 5, 'authToken', 'aedfe24c72493cee72acee55712e9790c728fa08072e60417087d43a4c5c5cac', '["*"]', NULL, '2024-02-13 23:05:15', '2024-02-13 23:05:15'),
	(287, 'App\\Models\\User', 5, 'authToken', '8e64da6e590454013bda5989ef2d5889b2f025d715c39d738cdf90d1eaf4d342', '["*"]', NULL, '2024-02-13 23:09:01', '2024-02-13 23:09:01'),
	(288, 'App\\Models\\User', 9, 'authToken', 'bbc490f89106f8c605ca12f31648669d6d942ba773e27612e25395fc8fb4ab0d', '["*"]', NULL, '2024-02-14 09:59:50', '2024-02-14 09:59:50'),
	(289, 'App\\Models\\User', 9, 'authToken', '1a672b84115178228cf1bec7784a7a1cbb2c9279171285e906e85a0422ddd709', '["*"]', NULL, '2024-02-14 10:02:36', '2024-02-14 10:02:36'),
	(290, 'App\\Models\\User', 5, 'authToken', '8ed4dbd127bc2821c6adcecccbeb156c5d20e5adc9482112d78b1b83016655f2', '["*"]', NULL, '2024-02-14 18:36:04', '2024-02-14 18:36:04'),
	(292, 'App\\Models\\User', 17, 'authToken', '27f38822cdd8a8c4db949344df7a97ff80c84fd0381b5b034a8f1d8244cd4e09', '["*"]', NULL, '2024-02-15 11:27:48', '2024-02-15 11:27:48'),
	(293, 'App\\Models\\User', 17, 'authToken', '5a97849425bee7cfbeab091ecfa562a97bdf3951c658c3159940f17bda5b6f0c', '["*"]', NULL, '2024-02-15 12:02:39', '2024-02-15 12:02:39'),
	(296, 'App\\Models\\User', 17, 'authToken', '037bae8053f9e3e31ccbedeb93099d232746c5340a603d0e77405556ff8c16f4', '["*"]', NULL, '2024-02-15 16:24:50', '2024-02-15 16:24:50'),
	(298, 'App\\Models\\User', 14, 'authToken', 'f5ac2b6d100bd71e4e89cfff5fa4566a5de77466688e6264e652812a42b7f3f7', '["*"]', NULL, '2024-02-15 16:48:03', '2024-02-15 16:48:03'),
	(299, 'App\\Models\\User', 9, 'authToken', 'f0b043b0de72fe86fae67704946298b56efbe8cf2694c2a6c4131b4052a7f1be', '["*"]', NULL, '2024-02-15 22:29:50', '2024-02-15 22:29:50'),
	(300, 'App\\Models\\User', 18, 'authToken', '4081bb962eff3cbb43001fdf317c2368edbb0067d62132463d998ad0cd8e2c63', '["*"]', NULL, '2024-02-16 09:28:45', '2024-02-16 09:28:45'),
	(301, 'App\\Models\\User', 9, 'authToken', 'b889f751f8c49186dc4a936c0cc1212b099ee3e91013c777dbaefd0eb45e4e50', '["*"]', NULL, '2024-02-16 10:24:04', '2024-02-16 10:24:04'),
	(302, 'App\\Models\\User', 9, 'authToken', '4418471e91093d54b52cbc4b7763e4526ad6739965ed901908c78848ba53593e', '["*"]', NULL, '2024-02-16 18:02:46', '2024-02-16 18:02:46'),
	(304, 'App\\Models\\User', 9, 'authToken', 'b63f1f1d425bdec3cfb2b5afd18836b85fb78c0747679a7d02a6a175a6a2e52a', '["*"]', NULL, '2024-02-17 17:03:35', '2024-02-17 17:03:35'),
	(306, 'App\\Models\\User', 5, 'authToken', 'cadbbe9d739fcd43f492dd98800d30aa91b7bc8bfad014174747c103cfd3a458', '["*"]', NULL, '2024-02-18 08:36:01', '2024-02-18 08:36:01'),
	(307, 'App\\Models\\User', 1, 'authToken', 'a51003495c73f97883f1dc1ef0912754cf33f6c3519a9fb71636a8aac9bc708a', '["*"]', NULL, '2024-02-18 08:40:17', '2024-02-18 08:40:17'),
	(308, 'App\\Models\\User', 5, 'authToken', '60170290854737dd1405287b4933f854ec3c204624df5b9954187d914f5e3c91', '["*"]', NULL, '2024-02-18 08:42:49', '2024-02-18 08:42:49'),
	(309, 'App\\Models\\User', 5, 'authToken', '3b167bd97f30eb02a520593d81d958d34bd1f021145ec3dbdc08b4fb737f4e0c', '["*"]', NULL, '2024-02-18 08:43:33', '2024-02-18 08:43:33'),
	(310, 'App\\Models\\User', 5, 'authToken', '4e4ab5b92c4fb40da790c490061751f72dfaef4dc54bc1ed075500a1b574049e', '["*"]', NULL, '2024-02-18 18:08:08', '2024-02-18 18:08:08'),
	(311, 'App\\Models\\User', 5, 'authToken', '029fd01e4b032ae64b7580a792600a3987324b2f21ab5e5c507a201816628015', '["*"]', NULL, '2024-02-18 18:11:13', '2024-02-18 18:11:13'),
	(312, 'App\\Models\\User', 5, 'authToken', '176ff0f49fdae4121aff70627bbb726097d43ece63f8db09e0bb6b1512096bcb', '["*"]', NULL, '2024-02-19 17:53:30', '2024-02-19 17:53:30'),
	(313, 'App\\Models\\User', 5, 'authToken', 'bbf9224bbe644787452c8c9002a98670a04e787e4cf2f59f63c4bb4d15bc7ea6', '["*"]', NULL, '2024-02-19 18:47:41', '2024-02-19 18:47:41'),
	(314, 'App\\Models\\User', 5, 'authToken', 'dc050cbe006abe1d81929fbbce13cd0cbb3ae0103163cae0ffa14f40ce03cf07', '["*"]', NULL, '2024-02-19 19:45:15', '2024-02-19 19:45:15'),
	(316, 'App\\Models\\User', 9, 'authToken', '8844eccc5b9eab444696b1084df7ca096edeb6f227269558c4e05b0dec63623e', '["*"]', NULL, '2024-02-20 12:10:38', '2024-02-20 12:10:38'),
	(317, 'App\\Models\\User', 5, 'authToken', 'f1ac4c2268dabf7e129a9f8076cbded9c55f94756b0a299aad702fc80c96921e', '["*"]', NULL, '2024-02-20 16:02:08', '2024-02-20 16:02:08'),
	(318, 'App\\Models\\User', 5, 'authToken', '518931918c70080e064cbb4c9cb037f20336005250acc7c3623a9215e5bf7f6a', '["*"]', NULL, '2024-02-20 16:09:01', '2024-02-20 16:09:01'),
	(319, 'App\\Models\\User', 5, 'authToken', '459ee4a08ea9b511d72c4f3516c9ae31bca2d9db81d1e16d41ef6e699f926f0d', '["*"]', NULL, '2024-02-20 16:12:55', '2024-02-20 16:12:55'),
	(320, 'App\\Models\\User', 9, 'authToken', 'dc52768f00dec60c60baebd639b2f4cc6b3483d81f8595b23280e17cc219fa80', '["*"]', NULL, '2024-02-20 17:15:49', '2024-02-20 17:15:49'),
	(321, 'App\\Models\\User', 9, 'authToken', '81cea06b1596c1f50d3f7521c3c934d57a6a1cb8e1d53788d712c69b7434f2bd', '["*"]', NULL, '2024-02-20 17:40:32', '2024-02-20 17:40:32'),
	(322, 'App\\Models\\User', 5, 'authToken', 'f80b71c59ae5db8474d45babd8e692b47019166c497c7d14a66125fa305e5370', '["*"]', NULL, '2024-02-20 18:14:40', '2024-02-20 18:14:40'),
	(323, 'App\\Models\\User', 5, 'authToken', '95cfb44296c70899910b4aaea5cb87d80ed98f1f2245ba84fc74155304b6b962', '["*"]', NULL, '2024-02-20 19:10:51', '2024-02-20 19:10:51'),
	(324, 'App\\Models\\User', 5, 'authToken', '70627a766c9230fca3d1f9413be213ad05fe580c4fd7337504c4792a3925204d', '["*"]', NULL, '2024-02-24 13:47:43', '2024-02-24 13:47:43'),
	(325, 'App\\Models\\User', 5, 'authToken', 'f1e5564cea6bdcab981486a5033fb2e29f6d1dc12ff3d18831d2092c2ff78e7f', '["*"]', NULL, '2024-02-24 13:47:43', '2024-02-24 13:47:43'),
	(326, 'App\\Models\\User', 5, 'authToken', 'ba34c6812b3c2da0447f833791d4441772c935906b94deb2b87ad5009807064c', '["*"]', NULL, '2024-02-24 13:47:43', '2024-02-24 13:47:43'),
	(327, 'App\\Models\\User', 5, 'authToken', '709e80d4818fa508d414d5dc3f91a3042c061bd47b5c7c907cc2ea5584065a22', '["*"]', NULL, '2024-02-24 13:48:08', '2024-02-24 13:48:08'),
	(328, 'App\\Models\\User', 5, 'authToken', '15dcf4c82b2cee351d05454a8888a205cdce1c907ed7abdd34e92ba1caf38c4e', '["*"]', NULL, '2024-02-24 13:48:14', '2024-02-24 13:48:14'),
	(330, 'App\\Models\\User', 5, 'authToken', '1b1d5b23c970acef7fc6cc9ec27655ade7b3505892ac97f773cb24cc70cbc25c', '["*"]', NULL, '2024-02-24 14:02:27', '2024-02-24 14:02:27'),
	(331, 'App\\Models\\User', 5, 'authToken', 'eb74ea19036ac7f636c61f456d9d0808c2f25b5089c7f71d31d6ef158c1beed3', '["*"]', NULL, '2024-02-24 14:27:58', '2024-02-24 14:27:58'),
	(332, 'App\\Models\\User', 5, 'authToken', '3d13c201ccddf44c31bfac5efe1f67873e36e33f47ab03adf5e8e3830fef22b0', '["*"]', NULL, '2024-02-24 14:28:49', '2024-02-24 14:28:49'),
	(333, 'App\\Models\\User', 5, 'authToken', 'ba4812ba315e3985556cce4c49118eea117cc453aec0e054021342e99d7d3b75', '["*"]', NULL, '2024-02-24 14:30:03', '2024-02-24 14:30:03'),
	(334, 'App\\Models\\User', 5, 'authToken', 'f083f86a7e31d338cb1894f637696c4601da4e8429fdaeaa9a25b771c60b08bf', '["*"]', NULL, '2024-02-24 14:39:34', '2024-02-24 14:39:34'),
	(335, 'App\\Models\\User', 5, 'authToken', '969ebb4f8280f02b5be3c49d0f63187067edfa1ead9f5cf935d87f4129eb1f32', '["*"]', NULL, '2024-02-24 14:42:32', '2024-02-24 14:42:32'),
	(336, 'App\\Models\\User', 5, 'authToken', 'ba3ddbdc012740e92bdf5e467063231c43fec544e711ad292dab2b5c06636fa4', '["*"]', NULL, '2024-02-24 14:56:47', '2024-02-24 14:56:47'),
	(338, 'App\\Models\\User', 9, 'authToken', '93969abe41a60aee482e27f3a6fad35041bd4bdcb5102731a2030aa50fb49da9', '["*"]', NULL, '2024-02-24 19:55:06', '2024-02-24 19:55:06'),
	(339, 'App\\Models\\User', 13, 'authToken', '8d6679251aacb307c2ebf9828875993071d3faccb05889f217bbf1bb19032698', '["*"]', NULL, '2024-02-25 05:31:45', '2024-02-25 05:31:45'),
	(340, 'App\\Models\\User', 9, 'authToken', '3f90f465b1c0be975ef157908c305e9659c37c266c637c786d2e0b8b9e6f8836', '["*"]', NULL, '2024-02-25 12:10:20', '2024-02-25 12:10:20'),
	(341, 'App\\Models\\User', 13, 'authToken', '30128fd65fd18e8f3acb3f2ca3e8edc5103f8f7675dbc46877c3660d6281fff3', '["*"]', NULL, '2024-02-25 20:21:50', '2024-02-25 20:21:50'),
	(343, 'App\\Models\\User', 9, 'authToken', 'c74aeb03d69e2997f1aaee40f45ed1d8189dac656f60d498ac878ffa7e90a203', '["*"]', NULL, '2024-02-26 08:47:19', '2024-02-26 08:47:19'),
	(344, 'App\\Models\\User', 5, 'authToken', '1d1d7012ad416213357d925de624f64cb623afd042528dae0faefc9cd9195082', '["*"]', NULL, '2024-02-28 12:42:15', '2024-02-28 12:42:15'),
	(346, 'App\\Models\\User', 20, 'authToken', '4517018af6b4fe9fcd62fe68bfe68d4e90e8618898cd654aa898930d7b9418bd', '["*"]', NULL, '2024-03-01 16:50:07', '2024-03-01 16:50:07'),
	(347, 'App\\Models\\User', 13, 'authToken', 'ca79077b9cd8f05576bc1043c2c6135ecbe8db7505df107d650151411791ea80', '["*"]', NULL, '2024-03-01 21:47:22', '2024-03-01 21:47:22'),
	(349, 'App\\Models\\User', 13, 'authToken', '372c26c34a687be1e36e0489b036970528f581a81b80a7f70ad513841686f6d3', '["*"]', NULL, '2024-03-01 21:52:18', '2024-03-01 21:52:18'),
	(352, 'App\\Models\\User', 20, 'authToken', 'fb87e12a3ff88c44909822067db6ec0f7d7993d1ac94f4862e27de02480f7f9d', '["*"]', NULL, '2024-03-02 14:43:15', '2024-03-02 14:43:15'),
	(354, 'App\\Models\\User', 9, 'authToken', 'a9f36428d03ae934e246769483df43d78cdc6234af10cccb87e7936f1f351fdf', '["*"]', NULL, '2024-03-04 17:02:27', '2024-03-04 17:02:27'),
	(355, 'App\\Models\\User', 9, 'authToken', '197121896b709f6b6a8dcd88671f118709104370b8081faca53c0ab1ab4530e4', '["*"]', NULL, '2024-03-04 17:07:46', '2024-03-04 17:07:46'),
	(356, 'App\\Models\\User', 20, 'authToken', '7060af70be9e78eda8a100aab7eee5c74a30ef129f1ca805cb64677432a4c281', '["*"]', NULL, '2024-03-05 07:56:55', '2024-03-05 07:56:55');
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.quizzes
CREATE TABLE IF NOT EXISTS `quizzes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pertanyaankuis` text,
  `jawaban` varchar(255) DEFAULT NULL,
  `jawab` varchar(255) DEFAULT NULL,
  `idLuka` varchar(255) DEFAULT NULL,
  `oleh` varchar(255) DEFAULT NULL,
  `idUser` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table diabetesapp.quizzes: ~0 rows (approximately)
/*!40000 ALTER TABLE `quizzes` DISABLE KEYS */;
/*!40000 ALTER TABLE `quizzes` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.sessions
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.sessions: ~0 rows (approximately)
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.transactions
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `food_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.transactions: ~0 rows (approximately)
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;

-- Dumping structure for table diabetesapp.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USER',
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `phoneNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table diabetesapp.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `roles`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `address`, `phoneNumber`, `city`, `created_at`, `updated_at`, `profile_photo_path`) VALUES
	(1, 'Irfan', 'kaisa.ads88@gmail.com', NULL, '$2y$10$VTYQT6.2auNvMBLlyORL2.2q8nAC.5TVqQr5FRUWitnz7QWCNzqWm', 'USER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2024-01-12 04:12:11', '2024-01-14 17:39:36', 'assets/user/wEfbcV2lw62Fp0DVPihqtPvrsy2rLutmqmyk4izy.png'),
	(5, 'admin', 'admin@unsap.ac.id', NULL, '$2y$10$zI8/eO5hIUFzwGkPXzIjoeH4G6ldjWLLIT4lGv5JT7i1MTMF2NDyK', 'USER', NULL, NULL, NULL, NULL, 'BDG', '12', 'SMD', '2024-01-14 18:01:30', '2024-01-14 18:31:48', 'assets/user/JFuhH0fYsQH38pHi4hJMyjmATMC3DhgVdNL94nic.jpg'),
	(6, 'Irfan', 'fadil.ads88@gmail.com', NULL, '$2y$10$EbG8sPfZa1F2.xytdYbTpe4kiCikN2cKaOmJ2LfpotHuS8NJT1mz2', 'USER', NULL, NULL, NULL, NULL, 'Bandung', '085220717928', 'Bandung', '2024-01-14 18:27:41', '2024-01-14 18:27:41', NULL),
	(7, 'Rangga', 'rangga@gmail.com', NULL, '$2y$10$gkbrXLjRghBE74TnfH0q1.4xvbW2092JNWdor1aWUp4DqmxWfITm.', 'USER', NULL, NULL, NULL, NULL, 'Jl. Angkrek Situ', '0812346789', 'Sumedang', '2024-01-15 14:08:42', '2024-01-15 14:08:44', 'assets/user/hHUpZsA3ewIzyyvZ48as0DqoaSyAhW3lYTqE4x3D.jpg'),
	(8, 'kaka', 'ka@gmail.com', NULL, '$2y$10$wEkZlXZa5WsdyZe3VCrtauCUjxLf3uMyPMv.GGliI2JUMjNiN11tW', 'USER', NULL, NULL, NULL, NULL, 'Bandung', '085220717928', 'bandung', '2024-01-15 14:10:55', '2024-01-15 14:10:56', 'assets/user/xU0BO0LEHvsQzZLrfUar94t9883YIWYGPGOSPQJ6.jpg'),
	(9, 'Maria', 'maria@gmail.com', NULL, '$2y$10$POHrYMqJOlKBFJp1twsaw.Nu6O6O6rC9lYdK7B3nQfJsGlh39aIhK', 'USER', NULL, NULL, NULL, NULL, 'Jl. Angkrek Situ', '08123456789', 'Sumedang', '2024-01-15 15:40:46', '2024-01-15 15:40:46', 'assets/user/Z6huPXStanZNKW3JxQsP9UiMEIN3rUfRUTYi8eLN.png'),
	(10, 'Deka Dia', 'diab@gmail.com', NULL, '$2y$10$BzLx/kxr9Myd3tdMxZ4gPepDVR1GtIREX6rF6GWKdRnHEb5icHfhq', 'USER', NULL, NULL, NULL, NULL, 'Jl. Angkrek', '0123456789', 'Sumedang', '2024-01-22 17:04:05', '2024-01-22 17:04:06', 'assets/user/gsBXK3dkSqHREDy0aaQe4Rx8MtMULupISUxZbcKo.jpg'),
	(11, 'Deka Diabetes', 'dekadiabetes@gmail.com', NULL, '$2y$10$PL8ndP.epxK8i//0nWznTul841.VzdMSOxTnMyHnedr0KirbhoPge', 'USER', NULL, NULL, NULL, NULL, 'Angkrek', '1234', 'Sumedang', '2024-01-22 17:15:20', '2024-01-22 17:15:20', 'assets/user/jhZEvSXDqUpRFVkI746YzaB5JQwHluGyc4HZXZs3.jpg'),
	(12, 'Dewi', 'dewiikawati349@gmail.com', NULL, '$2y$10$Xiu3M19CjgVZmFf86vI2teQVeGRlLGrVnVPZ5iooJUOKtukryw8Xu', 'USER', NULL, NULL, NULL, NULL, 'Sumedang', '082116772400', 'Sumedang', '2024-01-22 19:51:18', '2024-01-22 19:51:18', NULL),
	(13, 'Dewi Ikawati', 'dewiikawati1@gmail.com', NULL, '$2y$10$96raLZTd2Blv.74SJsmpVO9Pbtup4USgo.qwtH.ZK8zjOs.fROO9u', 'USER', NULL, NULL, NULL, NULL, 'Licin', '082116772400', 'Sumedang', '2024-01-26 08:34:34', '2024-01-26 08:34:38', 'assets/user/NsKuPMW3VJBc5Ue2v6jjlxO6kHyOJRvrXTaA52NO.png'),
	(14, 'Administrator', 'admin@gmail.com', NULL, '$2y$10$XITrvPnZNUSc1riM4v2oKeJXByBygVLAXXxJmxm7cRefD9Hz5he8W', 'ADMIN', NULL, NULL, 'ahjPjzhrkgrfkSCk5tizeaC7Ky0AyEuPkdu4ddpSQpWt79SqAXMRIrppQza2', NULL, 'Bandung', '-', 'Bandung', '2024-01-31 06:31:52', '2024-01-31 06:31:52', NULL),
	(15, 'dummy', 'dumy@gmail.com', NULL, '$2y$10$JbyUWEzvpsepXp9YWmpIteHIs6Bedqhv9mkusx61L3O8sJze2Gsm.', 'USER', NULL, NULL, NULL, NULL, 'Sumedang', '123', 'Sumedang', '2024-02-09 05:53:41', '2024-02-09 05:53:43', 'assets/user/noUjVxu7UoHEzVUI9Y6Fc9hbBuaSP0zmsjp5dGS9.jpg'),
	(16, 'Juariah', 'Yunitaonline123@gmail.com', NULL, '$2y$10$chkCKpPc7URi/p0BjwpYROdxETTthbEKvhwLCS2UyBy4RNU8SyIfC', 'USER', NULL, NULL, NULL, NULL, 'Tagog', '082120201236', 'Sumedang', '2024-02-12 06:50:36', '2024-02-12 06:50:36', NULL),
	(17, 'Test', 'yedistd8@gmail.com', NULL, '123456', 'ADMIN', NULL, NULL, NULL, NULL, 'Situraja', '085150980378', 'Sumedang', '2024-02-13 10:15:19', '2024-02-15 16:47:24', NULL),
	(18, 'Yedi', 'yd.tugas7@gmail.com', NULL, '$2y$10$1XHlMS4NmChtGE/uWtaSDOV0fjYVcLBnEAKmVJX0qkROctqnXY/bm', 'USER', NULL, NULL, NULL, NULL, 'Situraja', '085334690908', 'Sumedang', '2024-02-16 09:28:45', '2024-02-16 09:28:45', NULL),
	(19, 'Ibu Rokxxxxx', 'Rokaxxxx@gmail.com', NULL, '$2y$10$2nog6UtQ4wPB69I0xliFfOV/glS3rpavOfJdhVvT8tszkjku0jtrK', 'USER', NULL, NULL, NULL, NULL, 'Dusun surian', '085xxxxxxx', 'Sumedang', '2024-02-25 20:52:15', '2024-02-25 20:52:15', NULL),
	(20, 'Yedi Setiadi', 'yedistd9@gmail.com', NULL, '$2y$10$RNXt4R4s1Qwt1TsxgW/5G.7jv8u0FJTgDSiZcch3zYBOLQnGrc36i', 'USER', NULL, NULL, NULL, NULL, 'Situraja', '085179797497', 'Sumedang', '2024-03-01 16:50:07', '2024-03-01 16:50:07', NULL),
	(21, 'Tuti mulyati', 'Tuti.mulyati46@gmail.com', NULL, '$2y$10$yB6277KP7HeNcoLZVS1tbuIY5XSL7UyIFP2sk/mwZs7QlQtN70Qk2', 'USER', NULL, NULL, NULL, NULL, 'Cipada', '081395490846', 'Sumedang', '2024-03-01 21:50:37', '2024-03-01 21:50:37', NULL),
	(22, 'Juju', 'andi123@gmail.com', NULL, '$2y$10$q9g3N4BWcirsMd3A/q9pr.JdnOz1hfkrCKTsCX5Ilupk962pYzAYu', 'USER', NULL, NULL, NULL, NULL, 'Tanjungkerta', '082219185825', 'Sumedang', '2024-03-02 08:51:45', '2024-03-02 08:51:45', NULL),
	(23, 'Anom', 'Anom1@gmail.com', NULL, '$2y$10$KybRmGazkITqKym7SC3Q9uUQNbqZ/sF82p/dbXjNwIh7IEr9q1yXC', 'USER', NULL, NULL, NULL, NULL, 'Licin', '081804299177', 'Sumedang', '2024-03-04 12:49:51', '2024-03-04 12:49:51', NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;

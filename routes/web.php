<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Home;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('layouts.main');
}); */

Route::get('/', [AuthController::class, 'index'])->name('auth.index');
Route::post('/proses-login', [AuthController::class, 'authenticate'])->name('login.operator');
Route::get('/logout', [AuthController::class, 'logout'])->name('auth.logout');

Route::get('/dashboard', [Home::class, 'index'])->name('home.index');


Route::get('/list', [Home::class, 'list'])->name('home.list');
Route::get('/form', [Home::class, 'form'])->name('home.form');
Route::post('/pengguna', [Home::class, 'simpanPengguna'])->name('home.pengguna');

Route::get('/listgula', [Home::class, 'listgula'])->name('home.listgula');
Route::get('/listgulapasien', [Home::class, 'listgulapasien'])->name('home.listgulapasien');
Route::get('/formgula', [Home::class, 'formgula'])->name('home.formgula');
Route::post('/pilih-pasien', [Home::class, 'pilihpasien'])->name('home.pilihpasien');
Route::post('/pilih-pasien-luka', [Home::class, 'pilihpasienluka'])->name('home.pilihpasienluka');
Route::get('/data-pasien-luka', [Home::class, 'datapasienluka'])->name('home.datapasienluka');
Route::get('/data-pasien', [Home::class, 'datapasien'])->name('home.datapasien');

Route::post('/simpangula', [Home::class, 'simpangula'])->name('home.simpangula');
Route::post('/simpanluka', [Home::class, 'simpanluka'])->name('home.simpanluka');

Route::delete('/hapusgula/{id}', [Home::class, 'hapusgula'])->name('home.hapusgula');
Route::delete('/hapusluka/{id}', [Home::class, 'hapusluka'])->name('home.hapusluka');


Route::get('/listluka', [Home::class, 'listluka'])->name('home.listluka');
Route::get('/listlukapasien', [Home::class, 'listlukapasien'])->name('home.listlukapasien');
Route::get('/formluka', [Home::class, 'formluka'])->name('home.formluka');

Route::get('/listdiskusi', [Home::class, 'listdiskusi'])->name('home.listdiskusi');
Route::get('/formdiskusi', [Home::class, 'formdiskusi'])->name('home.formdiskusi');
Route::get('/balasdiskusi/{id}', [Home::class, 'formdiskusibalas'])->name('home.formdiskusibalas');


Route::post('/simpandiskusi', [Home::class, 'simpandiskusi'])->name('home.simpandiskusi');

Route::get('/listhasil', [Home::class, 'listhasil'])->name('home.listhasil');


<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Luka;
use Exception;
use Illuminate\Http\Request;

class LukaController extends Controller
{
    public function all(Request $request)
    {
        $id = $request->input('id');
        $limit = $request->input('limit', 1000);
        $idUser = $request->input('idUser');
       

        if($id)
        {
            $luka = Luka::find($id);
            if($luka)
                return ResponseFormatter::success(
                    $luka,
                    'Data berhasil diambil'
                );
            else
                return ResponseFormatter::error(
                    null,
                    'Data tidak ditemukan',
                    404
                );
        }

        $luka = Luka::query()->orderBy('id', 'desc');

        if($idUser)
            $luka->where('idUser', $idUser);

        return ResponseFormatter::success(
            $luka->paginate($limit),
            'Data list luka berhasil diambil'
        );
    }

    public function simpanLuka(Request $request)
    {
        try {
            $request->validate([
                'picturePath'=>'required|image|max:2048',
                'catatan'=>'required',
                'created_at'=>'required'
            ]);
            $file = $request->picturePath->store('assets/user', 'public');

            Luka::create([
                'picturePath'=>$file,
                'catatan'=>$request->catatan,
                'oleh'=>$request->oleh,
                'idUser'=>$request->idUser,
                'created_at'=>$request->created_at
            ]);

            return response()->json(['status'=>'200','success'=>'Data berhasil dimasukan']);


        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }

    }

    public function updateLuka(Request $request,$id)
    {
        try {
            $request->validate([
                'picturePath'=>'required|image|max:2048',
                'catatan'=>'required',
            ]);
            $file = $request->picturePath->store('assets/user', 'public');

            Luka::updateOrCreate(
                ['id'=>$id],
                [
                    'picturePath'=>$file,
                    'catatan'=>$request->catatan,
                    'oleh'=>$request->oleh,
                    'idUser'=>$request->idUser
                ]);
            
            return response()->json(['status'=>'200','success'=>'Data berhasil diupdate']);

        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }
    }

    public function deleteLuka($id)
    {
        try {
            Luka::find($id)->delete();
            return response()->json(['status'=>'200','success'=>'Data Luka dengan id='.$id.' Berhasil Dihapus !']);

        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }
    }
}

<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class AuthController extends Controller
{
    public function index()
    {
        return view('auth.index');
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();

        return redirect('/')->with('success', 'Anda Berhasil Logout ..');
    }

    public function authenticate(Request $request)
    {
        try {
            $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);

            $credentials = request(['email', 'password']);
            if (Auth::attempt($credentials)) {
                if (auth()->user()->roles=='ADMIN')
                {
                    return redirect()->intended('/dashboard')->with('warning', 'Hak Akses Sebagai Admin ...');
                }else
                {
                    return redirect()->intended('/')->with('error', 'Role Tidak Terdaftar..');
                }
                
            }else
            {
                return redirect('/')->with('error', 'User Tidak Dikenali..');  
            }
        } catch (Exception $error) {
            return redirect('/')->with('error', 'Terdapat Kesalahan'); 
        }

    }
}

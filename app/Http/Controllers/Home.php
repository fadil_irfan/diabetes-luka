<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use Exception;
use App\Models\Diabetes;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class Home extends Controller
{
    public function index()
    {
        return view('dashboard');
    }

    public function list(Request $request)
    {
        if($request->ajax()) {
            $getData=DB::table('users')
            ->select('users.*')
            ->get();
                $data=[];
                foreach ($getData as $item)
                {
                    $data[]=[
                        'id'=>$item->id,
                        'name'=>$item->name,
                        'email'=>$item->email,
                        'roles'=>$item->roles,
                        'phone'=>$item->phoneNumber,
                    ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);
        }
       
        return view('pengguna.index');
    }

    public function form()
    {
        return view('pengguna.fpengguna');
    }

    public function simpanPengguna(Request $request)
    {
        try 
        {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'max:8'],
            ]);

            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'address' => $request->address,
                'phoneNumber' => $request->phoneNumber,
                'city' => $request->city,
                'roles'=>$request->roles,
                'password' => Hash::make($request->password),
            ]);
            return response()->json(['status'=>'200','success'=>"Data Pengguna Berhasil Di Masukan .."]);

        } catch (Exception $e) {
            return response()->json(['status'=>'201','error'=>$e->getMessage()]);
            
        }

    }

    public function listgula(Request $request)
    {
        if($request->ajax()) {
            $getData=DB::table('diabetes')
            ->select('diabetes.*')
            ->get();
                $data=[];
                foreach ($getData as $item)
                {
                    $data[]=[
                        'id'=>$item->id,
                        'oleh'=>$item->oleh,
                        'waktu'=>$item->waktu,
                        'jenisPemeriksaan'=>$item->jenisPemeriksaan,
                        'nilai'=>$item->nilai,
                        'catatan'=>$item->catatan,
                        'tanggal'=>$item->created_at,
                    ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);
        }
       
        return view('gula.index');
    }

    public function listgulapasien(Request $request)
    {
        if($request->ajax()) {
            $getData=DB::table('diabetes')
            ->select('diabetes.*')
            ->where('idUser',session('id'))
            ->orderBy('id','desc')
            ->get();
                $data=[];
                foreach ($getData as $item)
                {
                    $data[]=[
                        'id'=>$item->id,
                        'oleh'=>$item->oleh,
                        'waktu'=>$item->waktu,
                        'jenisPemeriksaan'=>$item->jenisPemeriksaan,
                        'nilai'=>$item->nilai,
                        'catatan'=>$item->catatan,
                        'tanggal'=>$item->created_at,
                    ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);
        }
       
        
    }

    public function formgula()
    {
        $pasien = DB::table('users')
            ->select('users.*')
            ->orderBy('users.name')
            ->get();
        return view('pengguna.fpasien',[
            'pasien'=>$pasien
        ]);
    }

    public function pilihpasien(Request $request)
    {
        $pasien = DB::table('users')
        ->select('users.*')
        ->where('id',$request->idpasien)
        ->first();   
        $request->session()->put([
            'id' => $pasien->id,
            'name' => $pasien->name,
            'phoneNumber' => $pasien->phoneNumber,
        ]);
        return redirect()->intended('/data-pasien')->with('warning', 'Riwayat Data Gula Pasien');
        
    }
    public function datapasien(Request $request)
    {
        return view('gula.form');
    }

    public function simpangula(Request $request)
    {
        try 
        {
            $request->validate([
                'waktu'=>'required',
                'jenisPemeriksaan'=>'required',
                'nilai'=>'required',
                'oleh'=>'required',
                'idUser'=>'required',
                'created_at'=>'required'
            ]);

                DB::table('diabetes')->insert(
                        [
                            'waktu'=>$request->waktu,
                            'jenisPemeriksaan'=>$request->jenisPemeriksaan,
                            'nilai'=>$request->nilai,
                            'catatan'=>$request->catatan,
                            'oleh'=>$request->oleh,
                            'idUser'=>$request->idUser,
                            'created_at'=>$request->created_at

                        ]
                    );
            return response()->json(['status'=>'200','success'=>"Data Gula Pasien Berhasil Di Masukan .."]);

        } catch (Exception $e) {
            return response()->json(['status'=>'201','error'=>$e->getMessage()]);
            
        }
       
    }

    public function hapusgula($id)
    {
        DB::table('diabetes')->where('id', $id)->delete();
        return response()->json(['success'=>'Data Gula dengan id='.$id.' Berhasil Dihapus !']);

    }

    public function hapusluka($id)
    {
        $data=DB::table('lukas')->where('id', $id)->first();
        $file_path = public_path('storage/'.$data->picturePath);
        unlink($file_path);
        DB::table('lukas')->where('id', $id)->delete();
        return response()->json(['success'=>'Data Luka dengan id='.$id.' Berhasil Dihapus !']);

    }

    public function listluka(Request $request)
    {
        if($request->ajax()) {
            $getData=DB::table('lukas')
            ->select('lukas.*')
            ->get();
                $data=[];
                foreach ($getData as $item)
                {
                    $data[]=[
                        'id'=>$item->id,
                        'picturePath'=>$item->picturePath,
                        'catatan'=>$item->catatan,
                        'oleh'=>$item->oleh,
                        'tanggal'=>$item->created_at,
                    ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);
        }
       
        return view('luka.index');
    }

    public function listlukapasien(Request $request)
    {
        if($request->ajax()) {
            $getData=DB::table('lukas')
            ->select('lukas.*')
            ->where('idUser',session('id'))
            ->orderBy('id','desc')
            ->get();
                $data=[];
                foreach ($getData as $item)
                {
                    $data[]=[
                        'id'=>$item->id,
                        'picturePath'=>$item->picturePath,
                        'catatan'=>$item->catatan,
                        'oleh'=>$item->oleh,
                        'tanggal'=>$item->created_at,
                    ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);
        }
       
       
    }

    public function formluka()
    {
        $pasien = DB::table('users')
        ->select('users.*')
        ->orderBy('users.name')
        ->get();
    return view('pengguna.fpasienluka',[
        'pasien'=>$pasien
    ]);
    }
    public function pilihpasienluka(Request $request)
    {
        $pasien = DB::table('users')
        ->select('users.*')
        ->where('id',$request->idpasien)
        ->first();   
        $request->session()->put([
            'id' => $pasien->id,
            'name' => $pasien->name,
            'phoneNumber' => $pasien->phoneNumber,
        ]);
        return redirect()->intended('/data-pasien-luka')->with('warning', 'Riwayat Data Luka Pasien');
       
    }

    public function datapasienluka(Request $request)
    {
        return view('luka.form');
    }
    public function simpanluka(Request $request)
    {
        try {
            $request->validate([
                'picturePath'=>'required|image|max:2048',
                'catatan'=>'required',
                'created_at'=>'required'
            ]);
            $file = $request->picturePath->store('assets/user', 'public');
            DB::table('lukas')->insert(
                [
                    'picturePath'=>$file,
                    'catatan'=>$request->catatan,
                    'oleh'=>$request->oleh,
                    'idUser'=>$request->idUser,
                    'created_at'=>$request->created_at
                ]
            );

            return response()->json(['status'=>'200','success'=>'Data Luka Pasien berhasil dimasukan']);


        } catch (Exception $error) {
            return response()->json(['status'=>'201','error'=>$error->getMessage()]);
        }

    }

    public function listdiskusi(Request $request)
    {
        if($request->ajax()) {
            $getData=DB::table('diskusis')
            ->select('diskusis.*')
            ->get();
                $data=[];
                foreach ($getData as $item)
                {
                    $data[]=[
                        'id'=>$item->id,
                        'oleh'=>$item->oleh,
                        'diskusi'=>$item->diskusi,
                        'tanggal'=>$item->created_at,
                        'jumlahPercakapan'=>ResponseFormatter::getDiskusi($item->id),
                    ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);
        }
       
        return view('diskusi.index');
    }

    public function formdiskusi()
    {
        return view('diskusi.form');
    }

    public function formdiskusibalas($id)
    {
        $getData=DB::table('diskusis')
        ->select('diskusis.*')
        ->where('toUser',$id)
        ->get();
        return view('diskusi.formbalas',[
            'listDiskusi'=>$getData
        ]);
    }

    public function listhasil(Request $request)
    {
        if($request->ajax()) {
            $getData= Diabetes::select(DB::raw('
            jenisPemeriksaan,
            round(avg(nilai),2) as rata,
            idUser
            '))
            ->groupBy('jenisPemeriksaan','idUser')
            ->orderBy('idUser')
            ->get();
                $data=[];
                foreach ($getData as $item)
                {
                    $data[]=[
                        'jenisPemeriksaan'=>$item->jenisPemeriksaan,
                        'rata'=>$item->rata,
                        'idUser'=>ResponseFormatter::cekNamaUser($item->idUser),
                        'hasil'=>ResponseFormatter::finalHasil($item->jenisPemeriksaan,$item->rata)
                    ];
            }

            return Response()->json([
                'error_code'=>0,
                'error_desc'=>'',
                'data'=>$data,
                'message'=>'fetch data berhasil'
            ], 200);
        }
        return view('hasil.index');
    }

    public function simpandiskusi(Request $request)
    {
        try 
        {
            $request->validate([
                'diskusi'=>'required',
                'id'=>'required',
            ]);

                DB::table('diskusis')->insert(
                        [
                            'diskusi'=>$request->diskusi,
                            'oleh'=>ResponseFormatter::getUser(auth()->user()->id),
                            'idUser'=>auth()->user()->id,
                            'toUser'=>$request->id,
                            'created_at'=>Carbon::now(),
                            'updated_at'=>Carbon::now()

                        ]
                    );
            return response()->json(['status'=>'200','id'=>$request->id,'success'=>"Diskusi Pasien Berhasil Di Masukan .."]);

        } catch (Exception $e) {
            return response()->json(['status'=>'201','error'=>$e->getMessage()]);
            
        }
       
    }
}

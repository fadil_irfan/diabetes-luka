<?php

namespace App\Helpers;

use App\Models\Diabetes;
use App\Models\User;
use Illuminate\Support\Facades\DB;

/**
 * Format response.
 */
class ResponseFormatter
{
    /**
     * API Response
     *
     * @var array
     */
    protected static $response = [
        'meta' => [
            'code' => 200,
            'status' => 'success',
            'message' => null,
        ],
        'data' => null,
    ];

    /**
     * Give success response.
     */
    public static function success($data = null, $message = null)
    {
        self::$response['meta']['message'] = $message;
        self::$response['data'] = $data;

        return response()->json(self::$response, self::$response['meta']['code']);
    }

    /**
     * Give error response.
     */
    public static function error($data = null, $message = null, $code = 400)
    {
        self::$response['meta']['status'] = 'error';
        self::$response['meta']['code'] = $code;
        self::$response['meta']['message'] = $message;
        self::$response['data'] = $data;

        return response()->json(self::$response, self::$response['meta']['code']);
    }

    public static function cekNilai($jenis)
    {
        $diabetes = Diabetes::select(DB::raw('
                    jenisPemeriksaan,
                    round(avg(nilai),2) as rata,
                    idUser
                    '))
                    ->where('jenisPemeriksaan',$jenis)
                    ->groupBy('jenisPemeriksaan')
                    ->first();
            if (!$diabetes)
            {
                return '0';
            }else
            {
                return $diabetes['rata'];
            }

    }

    public static function cekNamaUser($id)
    {
        $namauser = User::select(DB::raw('
                    name
                    '))
                    ->where('id',$id)
                    ->first();
            if (!$namauser)
            {
                return 'Tidak Ditemukan';
            }else
            {
                return $namauser['name'];
            }

    }

    public static function generateHasil($gds,$gdp,$gdd,$hb)
    {
        if (($gds>200) || ($gdp>126) || ($gdd>180) || ($hb>7))
        {
            $hasil="Diabetes";
        }else if (($gds==0) || ($gdp>100) || ($gdd>140) || ($hb>6.5))
        {
            $hasil="Pre Diabetes";
        }else if (($gds<200) || ($gdp<100) || ($gdd<140) || ($hb<6.5))
        {
            $hasil="Normal";
        }

            return $hasil;
    }

    public static function finalHasil($gds,$nilai)
    {
        $hasil="";
        if ($gds=="Gula Darah Sewaktu")
        {
            if ($nilai>=200)
            {
                $hasil="Hiperglikemi";
            } else if ($nilai<=90)
            {
                $hasil="Hipoglikemia";
            } else{
                $hasil="Normal";
            }

        }

        if ($gds=="Gula Darah Puasa")
        {
            if ($nilai>=126)
            {
                $hasil="Hiperglikemi";
            }else if ($nilai<=70) {
                $hasil="Hipoglikemia";
            }else
            {
                $hasil="Normal";
            }

        }

        if ($gds=="GD 2 Jam Setelah Makan")
        {
            if ($nilai>180)
            {
                $hasil="Diabetes";
            }else if ($nilai>140) {
                $hasil="Pre Diabetes";
            }else
            {
                $hasil="Normal";
            }

        }

        if ($gds=="HbA1c")
        {
            if ($nilai>7)
            {
                $hasil="Diabetes";
            }else if ($nilai>6.5) {
                $hasil="Pre Diabetes";
            }else
            {
                $hasil="Normal";
            }

        }
            return $hasil;
    }

    public static function getUser($id)
    {
        $user = User::select(DB::raw('
                    name
                    '))
                    ->where('id',$id)
                    ->first();
            if (!$user)
            {
                return 'Noname';
            }else
            {
                return $user['name'];
            }

    }

    public static function getDiskusi($id)
    {
        $getData=DB::table('diskusis')
                    ->select('diskusis.*')
                    ->where('toUser',$id)
                    ->count();
        return $getData;


    }
}

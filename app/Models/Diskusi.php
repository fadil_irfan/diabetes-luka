<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Diskusi extends Model
{
    use HasFactory;
    protected $fillable = [
        'diskusi',
        'oleh',
        'idUser'
    ];

    
    public function getCreatedAtAttribute($created_at)
    {
        return Carbon::parse($created_at)
            ->locale('id')->settings(['formatFunction' => 'translatedFormat'])->format('l, j F Y ; h:i a');
    }
    public function getUpdatedAtAttribute($updated_at)
    {
        return Carbon::parse($updated_at)
            ->locale('id')->settings(['formatFunction' => 'translatedFormat'])->format('l, j F Y ; h:i a');
    }
}

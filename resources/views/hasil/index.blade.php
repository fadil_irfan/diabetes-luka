@extends('layouts.main')

@section('title', 'Manajemen Gula Darah')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
<h4 class="py-3 mb-2"><span class="text-muted fw-light">Hasil Diabetes</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
  <div class="row card-header">
    <div class="col-xl">

    </div>

  </div>


  <div class="table-responsive text-nowrap">
    <div class="row card-header">
        <div class="col-xl">
            <table id="refTabel" class="expandable-table" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>IdUser</th>
                        <th>Jenis Pemeriksaan</th>
                        <th>Nilai</th>
                        <th>Hasil</th>
                        {{-- <th>Gula Darah Sewaktu</th>
                        <th>Gula Darah Puasa</th>
                        <th>GD 2 Jam Setelah Makan</th>
                        <th>HbA1c</th>
                        <th>Hasil Diabetes</th> --}}
                    </tr>
                </thead>
            </table>
        </div>
    </div>




  </div>
</div>
<!--/ Basic Bootstrap Table -->

</div>
@endsection

@push('page-stylesheet')
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>


    $(document).ready(function() {
        $.noConflict();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

        let table = $('#refTabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: 'listhasil'
            },
            columns: [
                { data: null, orderable: false, searchable: false },
                { data: 'idUser' },
                { data: 'jenisPemeriksaan' },
                { data: 'rata' },
                { data: 'hasil' }
            ],
            'columnDefs': [
                {
                    "targets": 4,
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                            if (data=="Normal")
                            {
                                return "<a href='javascript:void(0)' class='btn btn-sm btn-success' >Normal</a>";
                            }else if (data=="Hiperglikemi")
                            {
                                return "<a href='javascript:void(0)' class='btn btn-sm btn-danger' >Hiperglikemi</a>";
                            }else
                            {
                                return "<a href='javascript:void(0)' class='btn btn-sm btn-warning' >Hipoglikemia</a>";
                            }

                    }
                }
            ]

        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i+1;
            });
        });

    });



</script>
<script>
@endpush

@extends('layouts.main')

@section('title', 'Form Balas Diskusi')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4"><span class="text-muted fw-light">Form Balas Diskusi</h4>

    <!-- Basic Layout & Basic with Icons -->
    <div class="row">
      <!-- Basic Layout -->
      <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
          </div>
          <div class="card-body">
            
              @foreach ($listDiskusi as $item) 
              @if ($item->id==request()->segment(2))
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-message">{{$item->oleh}} : </label>
                <div class="col-sm-10">
                  <h4 class="bubble">&nbsp&nbsp&nbsp{{$item->diskusi}} </h4>
                </div>
              </div>
              @else
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-message">{{$item->oleh}} : </label>
                <div class="col-sm-10">
                  <h4 class="bubble-right">&nbsp&nbsp&nbsp{{$item->diskusi}} </h4>
                </div>
              </div>
              @endif
              
                  
            @endforeach

              
             
            <form id="MabaForm" name="MabaForm">
             
              <input class="form-control" type="hidden" value="{{ request()->segment(2) }}" name="id">
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-message">Pesan</label>
                <div class="col-sm-10">
                  <textarea
                    id="basic-default-message"
                    class="form-control"
                    placeholder="Masukan Pesan Anda Disini"
                    rows="10"
                    name="diskusi"
                    aria-describedby="basic-icon-default-message2"></textarea>
                </div>
              </div>
              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" id="saveBtn" class="btn btn-primary">Kirim</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- / Content -->
@endsection

@push('page-stylesheet')
<style>
  .bubble {
      position: relative;
      background: #5fba6e;
      color: #FFFFFF;
      font-family: Arial;
      font-size: 14px;
      line-height: 50px;
      text-align: left;
      width: 250px;
      height: auto;
      border-radius: 10px;
      padding: 0px;
      
  }
  .bubble:after {
      content: '';
      position: absolute;
      display: block;
      width: 0;
      z-index: 1;
      border-style: solid;
      border-width: 0 0 20px 20px;
      border-color: transparent transparent #5fba6e transparent;
      top: 50%;
      left: -20px;
      margin-top: -10px;
  }
  
  </style>

<style>
  .bubble-right {
      position: relative;
      right: -200;
      background: #5fba6e;
      color: #FFFFFF;
      font-family: Arial;
      font-size: 14px;
      line-height: 50px;
      text-align: left;
      width: 250px;
      height: auto;
      border-radius: 10px;
      padding: 0px;
      margin-left:300px;


  }
  .bubble-right:after {
      content: '';
      position: absolute;
      display: block;
      width: 0;
      z-index: 1;
      border-style: solid;
      border-width: 20px 0 0 20px;
      border-color: transparent transparent transparent #5fba6e;
      top: 50%;
      right: -20px;
      margin-top: -10px;
  }
  
  </style>
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
  $(function () {
    $.noConflict();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });


    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $('#saveBtn').html('Mengirim Data ..');

        $.ajax({
          data: $('#MabaForm').serialize(),
          url: "/simpandiskusi",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              if (data.status=='200')
            {
                $('#MabaForm').trigger("reset");
              Swal.fire(
                  'Tambah Data Diskusi Berhasil',
                  'Silahkan klik tombol OK ' + data.success,
                  'success'
              ).then(function (result) {
              if (result.value) {
                window.location = "" + data.id ;
              }
          })
            }else
            {
                Swal.fire(
                  'Terdapat Kesalahan',
                  data.error,
                  'error'
              )
            }
              $('#saveBtn').html('Simpan');

      
          },
          error: function (data) {
              console.log('Error:', data);
              Swal.fire(
                  'Terdapat Kesalahan',
                  data.responseJSON.message,
                  'error'
              )
              $('#saveBtn').html('Simpan');
          }
      });
    });


    

  });

 
</script>
@endpush

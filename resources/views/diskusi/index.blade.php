@extends('layouts.main')

@section('title', 'Manajemen Diskusi')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
<h4 class="py-3 mb-2"><span class="text-muted fw-light">Manajemen Diskusi</h4>

<!-- Basic Bootstrap Table -->
<div class="card">
  <div class="row card-header">
    <div class="col-xl">
        <a href="{{ url('formdiskusi') }}" class="btn btn-primary">Tambah</a>
    </div>

  </div>


  <div class="table-responsive text-nowrap">
    <div class="row card-header">
        <div class="col-xl">
            <table id="refTabel" class="expandable-table" style="width:100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Isi Percakapan</th>
                        <th>Tanggal</th>
                        <th>Jumlah Percakapan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>




  </div>
</div>
<!--/ Basic Bootstrap Table -->

</div>
@endsection

@push('page-stylesheet')
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>


    $(document).ready(function() {
        $.noConflict();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
    });

        let table = $('#refTabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: 'listdiskusi'
            },
            columns: [
                { data: null, orderable: false, searchable: false },
                { data: 'oleh' },
                { data: 'diskusi' },
                { data: 'tanggal' },
                { data: 'jumlahPercakapan' },
                { data: 'id' },
            ],
            'columnDefs': [
                {
                    "targets": 4,
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                            return data;
                    }
                },
                {
                    "targets": 5,
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                            
                            return "<a href='balasdiskusi/" + data + "' class='btn btn-sm btn-primary' >Balas Pesan</a>";
                    }
                }
            ]

        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i+1;
            });
        });
        $('#refTabel').on('click', '.deleteUser', function () {

var Customer_id = $(this).data("id");
Swal.fire({
      icon: 'question',
      title: 'Apakah akan menghapus member ?',
      showCancelButton: true,
      cancelButtonText:'Tidak',
      confirmButtonText: 'Ya',
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
      $.ajax({
          type: "DELETE",
          url: "member/hapus"+'/'+Customer_id,
          success: function (data) {
                table.ajax.url('member').load();
              Swal.fire(data.success, '', 'success')
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });

  } else if (result.isDenied) {
      Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
  }
})


});
    });



</script>
<script>
@endpush

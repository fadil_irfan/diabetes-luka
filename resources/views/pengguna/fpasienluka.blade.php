@extends('layouts.main')

@section('title', 'Form Pilih Pasien')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4"><span class="text-muted fw-light">Form Pilih Pasien</h4>

    <!-- Basic Layout & Basic with Icons -->
    <div class="row">
      <!-- Basic Layout -->
      <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
          </div>
          <div class="card-body">
            <form action="{{ url('pilih-pasien-luka') }}" method="POST">
              @csrf
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Pilih Pasien : </label>
                <div class="col-sm-6">
                  <select class="js-example-basic-single form-control" name="idpasien" >
                    @foreach ($pasien as $pasien)
                    <option value="{{ $pasien->id }}">
                {{ $pasien->name.' - '.$pasien->phoneNumber }}
                    </option>
                @endforeach
                  </select>
                 
                </div>
              </div>
              
              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Pilih</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- / Content -->
@endsection

@push('page-stylesheet')
<script>
  
</script>
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
   var jq = jQuery.noConflict();
jq(document).ready(function() {
        jq('.js-example-basic-single').select2({
          theme: "classic"
        });
});
</script>
@endpush

@extends('layouts.main')

@section('title', 'Form Pengguna')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4"><span class="text-muted fw-light">Form Pengguna</h4>

    <!-- Basic Layout & Basic with Icons -->
    <div class="row">
      <!-- Basic Layout -->
      <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
          </div>
          <div class="card-body">
            <form id="FormPengguna">
              
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Nama</label>
                <div class="col-sm-10">
                  <input type="text" name="name" class="form-control" id="basic-default-name" placeholder="Masukan Nama" />
                </div>
              </div>
              
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-email">Email </label>
                <div class="col-sm-10">
                  <div class="input-group input-group-merge">
                    <input
                      type="text"
                      id="basic-default-email"
                      class="form-control"
                      placeholder="Masukan Email Pengguna"
                      name="email"
                      aria-describedby="basic-default-email2" />
                    
                  </div>
                  
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Password</label>
                <div class="col-sm-10">
                  <input
                      type="text"
                      id="password"
                      class="form-control"
                      name="password"
                      placeholder="Masukan Password"
                      aria-describedby="password" />
                    
                </div>
              </div>
              
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-phone">Nomor HP</label>
                <div class="col-sm-10">
                  <input
                    type="text"
                    id="basic-default-phone"
                    class="form-control phone-mask"
                    placeholder="08565xxxxx"
                    name="phoneNumber"
                    aria-describedby="basic-default-phone" />
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Alamat</label>
                <div class="col-sm-10">
                  <input type="text" name="address" class="form-control" id="basic-default-name" placeholder="Masukan Alamat" />
                </div>
              </div>

              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Kota</label>
                <div class="col-sm-10">
                  <input type="text" name="city" class="form-control" id="basic-default-name" placeholder="Masukan Kota" />
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-company">Role User</label>
                <div class="col-sm-10">
                  <select class="form-select" id="exampleFormControlSelect1" aria-label="Default select example" name="roles">
                    <option value="ADMIN">Administrator</option>
                    <option value="USER">Pasien</option>
                  </select>
                </div>
              </div>
              
              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button id="save" class="btn btn-primary">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- / Content -->
@endsection

@push('page-stylesheet')
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
  $(function () {
    $.noConflict();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });


$('#FormPengguna').submit(function (e) {
        e.preventDefault();
        $('#save').html('Mengirim Data ..');
      
        $.ajax({
          data: new FormData(this),
          url: "pengguna",
          method: "POST",
          dataType: 'json',
          contentType:false,
          cache:false,
          processData:false,
          success: function (data) {
              $('#FormPengguna').trigger("reset");
              Swal.fire(
                  data.success,
                  'Data Berhasil Disimpan..',
                  'success'
              ).then(function (result) {
              if (result.value) {
                window.location = "list"; 
              }
          })
              
              $('#saveSyarat').html('Simpan');
      
          },
          error: function (data) {
              //console.log('Error:', data);
              Swal.fire(
                  'Terdapat Kesalahan',
                  data.responseJSON.message,
                  'error'
              )
              $('#save').html('Simpan');
          }
      });
    });

 


  });


    

  


</script>
@endpush

 <!-- Menu -->

 <aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
      <a href="#" class="app-brand-link">
        <span class="app-brand-logo demo">
          <svg width="100" height="100" viewBox="0 0 72 69" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M36 0C16.1184 0 0 14.3421 0 32.0357C0 39.6688 3.01078 46.6674 8.01562 52.172C6.25219 59.9206 0.379688 66.8438 0.309375 66.9208C0.158283 67.0956 0.0572439 67.315 0.018736 67.552C-0.0197719 67.7889 0.00593776 68.0331 0.09269 68.2542C0.179442 68.4754 0.323437 68.6639 0.506893 68.7964C0.690349 68.9289 0.90523 68.9997 1.125 69C10.4428 69 17.4375 64.1099 20.8969 61.0866C25.7262 63.069 30.8426 64.0802 36 64.0714C55.883 64.0714 72 49.7293 72 32.0357C72 14.3421 55.883 0 36 0ZM49.5 35.7321C49.5 36.0589 49.3815 36.3723 49.1705 36.6034C48.9595 36.8345 48.6734 36.9643 48.375 36.9643H40.5V45.5893C40.5 45.9161 40.3815 46.2295 40.1705 46.4605C39.9595 46.6916 39.6734 46.8214 39.375 46.8214H32.625C32.3266 46.8214 32.0405 46.6916 31.8295 46.4605C31.6185 46.2295 31.5 45.9161 31.5 45.5893V36.9643H23.625C23.3266 36.9643 23.0405 36.8345 22.8295 36.6034C22.6185 36.3723 22.5 36.0589 22.5 35.7321V28.3393C22.5 28.0125 22.6185 27.6991 22.8295 27.468C23.0405 27.237 23.3266 27.1071 23.625 27.1071H31.5V18.4821C31.5 18.1554 31.6185 17.842 31.8295 17.6109C32.0405 17.3798 32.3266 17.25 32.625 17.25H39.375C39.6734 17.25 39.9595 17.3798 40.1705 17.6109C40.3815 17.842 40.5 18.1554 40.5 18.4821V27.1071H48.375C48.6734 27.1071 48.9595 27.237 49.1705 27.468C49.3815 27.6991 49.5 28.0125 49.5 28.3393V35.7321Z" fill="#019874"/>
            </svg>
            
            
        </span>
        <span class="app-brand-text demo menu-text fw-bold ms-2">DEKA</span>
      </a>

      <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
        <i class="bx bx-chevron-left bx-sm align-middle"></i>
      </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
      <!-- Dashboards -->
      
      <li class="menu-item {{ (request()->segment(1) == 'dashboard') ? 'active' : '' }}">
        <a
          href="{{ url('dashboard') }}"
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-home-circle"></i>
          <div data-i18n="Email">Dashboard</div>
         
        </a>
      </li>
      {{-- <li class="menu-item {{ (request()->segment(1) == 'list') ? 'active open' : '' }}">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
          <i class="menu-icon tf-icons bx bx-universal-access"></i>
          <div data-i18n="Dashboards">Master Data</div>
          
        </a>
        <ul class="menu-sub">
          <li class="menu-item {{ (request()->segment(1) == 'list') ? 'active' : '' }}">
            <a
              href="{{ url('list') }}"
              class="menu-link">
              <div data-i18n="CRM">Data Pengguna</div>
              
            </a>
          </li>
          {{-- <li class="menu-item">
            <a href="index.html" class="menu-link">
              <div data-i18n="Analytics">Data Dokter</div>
            </a>
          </li>
          <li class="menu-item">
            <a
              href="https://demos.themeselection.com/sneat-bootstrap-html-admin-template/html/vertical-menu-template/app-ecommerce-dashboard.html"
              target="_blank"
              class="menu-link">
              <div data-i18n="eCommerce">Perawat</div>
              
            </a>
          </li>
        </ul>
      </li> --}}
      <li class="menu-item {{ (request()->segment(1) == 'list') || (request()->segment(1) == 'form') ? 'active' : '' }}">
        <a
        href="{{ url('list') }}"
        
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-male-female"></i>
          <div data-i18n="Email">Pengguna</div>
         
        </a>
      </li>
      <li class="menu-item {{ (request()->segment(1) == 'listgula') || (request()->segment(1) == 'formgula') ? 'active' : '' }}">
        <a
        href="{{ url('listgula') }}"
        
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-scatter-chart"></i>
          <div data-i18n="Email">Gula Darah</div>
         
        </a>
      </li>

      <li class="menu-item {{ (request()->segment(1) == 'listluka') || (request()->segment(1) == 'formluka') ? 'active' : '' }}">
        <a
        href="{{ url('listluka') }}"
      
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-cross"></i>
          <div data-i18n="Email">Luka</div>
         
        </a>
      </li>

      <li class="menu-item {{ (request()->segment(1) == 'listhasil') ? 'active' : '' }}">
        <a
          href="{{ url('listhasil') }}"
          
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-notepad"></i>
          <div data-i18n="Email">Hasil Diabetes</div>
         
        </a>
      </li>

      <li class="menu-item {{ (request()->segment(1) == 'listdiskusi') || (request()->segment(1) == 'formdiskusi') ||(request()->segment(1) == 'formdiskusibalas') ? 'active' : '' }}">
        <a
          href="{{ url('listdiskusi') }}"
          
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-chat"></i>
          <div data-i18n="Email">Diskusi</div>
         
        </a>
      </li>

      <li class="menu-item">
        <a
          href="{{ url('/') }}"
          class="menu-link">
          <i class="menu-icon tf-icons bx bx-log-out"></i>
          <div data-i18n="Email">Keluar</div>
         
        </a>
      </li>

     
      

      

     
    </ul>
  </aside>
  <!-- / Menu -->
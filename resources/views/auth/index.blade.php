<!DOCTYPE html>

<html
  lang="en"
  class="light-style layout-wide customizer-hide"
  dir="ltr"
  data-theme="theme-default"
  data-assets-path="../assets/"
  data-template="vertical-menu-template-free">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>Deka | Diabetes dan Luka</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="../assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link
      href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
      rel="stylesheet" />

    <link rel="stylesheet" href="../assets/vendor/fonts/boxicons.css" />

    <!-- Core CSS -->
    <link rel="stylesheet" href="../assets/vendor/css/core.css" class="template-customizer-core-css" />
    <link rel="stylesheet" href="../assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="../assets/css/demo.css" />

    <!-- Vendors CSS -->
    <link rel="stylesheet" href="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />

    <!-- Page CSS -->
    <!-- Page -->
    <link rel="stylesheet" href="../assets/vendor/css/pages/page-auth.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" 
       href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    
      <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    <!-- Helpers -->
    <script src="../assets/vendor/js/helpers.js"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="../assets/js/config.js"></script>
  </head>

  <body>
    <!-- Content -->

    <div class="container-xxl">
      <div class="authentication-wrapper authentication-basic container-p-y">
        <div class="authentication-inner">
          <!-- Register -->
          <div class="card">
            <div class="card-body">
              <!-- Logo -->
              <div class="app-brand justify-content-center">
                <a href="#" class="app-brand-link gap-2">
                  <span class="app-brand-logo demo">
                    <svg width="118" height="113" viewBox="0 0 118 113" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M59 0C26.4163 0 0 23.4879 0 52.4643C0 64.9649 4.93434 76.4263 13.1367 85.4411C10.2466 98.1309 0.622266 109.469 0.507031 109.595C0.259408 109.881 0.0938164 110.24 0.0307062 110.629C-0.0324039 111.017 0.00973132 111.416 0.151909 111.779C0.294086 112.141 0.530078 112.45 0.830741 112.667C1.1314 112.884 1.48357 113 1.84375 113C17.1146 113 28.5781 104.992 34.2477 100.04C42.1623 103.287 50.5476 104.943 59 104.929C91.586 104.929 118 81.4407 118 52.4643C118 23.4879 91.586 0 59 0ZM81.125 58.5179C81.125 59.053 80.9307 59.5663 80.585 59.9447C80.2392 60.3231 79.7702 60.5357 79.2812 60.5357H66.375V74.6607C66.375 75.1959 66.1807 75.7091 65.835 76.0876C65.4892 76.466 65.0202 76.6786 64.5312 76.6786H53.4688C52.9798 76.6786 52.5108 76.466 52.165 76.0876C51.8193 75.7091 51.625 75.1959 51.625 74.6607V60.5357H38.7188C38.2298 60.5357 37.7608 60.3231 37.415 59.9447C37.0693 59.5663 36.875 59.053 36.875 58.5179V46.4107C36.875 45.8755 37.0693 45.3623 37.415 44.9839C37.7608 44.6054 38.2298 44.3929 38.7188 44.3929H51.625V30.2679C51.625 29.7327 51.8193 29.2194 52.165 28.841C52.5108 28.4626 52.9798 28.25 53.4688 28.25H64.5312C65.0202 28.25 65.4892 28.4626 65.835 28.841C66.1807 29.2194 66.375 29.7327 66.375 30.2679V44.3929H79.2812C79.7702 44.3929 80.2392 44.6054 80.585 44.9839C80.9307 45.3623 81.125 45.8755 81.125 46.4107V58.5179Z" fill="#019874"/>
                      </svg>
                      
                      
                  </span>
                  <span class="app-brand-text demo text-body fw-bold">Diabetes Luka</span>
                </a>
              </div>
              <!-- /Logo -->
              <h4 class="mb-2">Selamat Datang</h4>
              <p class="mb-4">Silahkan Masukan Email dan Password .. </p>

              <form id="formAuthentication" class="mb-3" action="{{ url('proses-login') }}" method="POST">
                @csrf
                <div class="mb-3">
                  <label for="email" class="form-label">Email</label>
                  <input
                    type="text"
                    class="form-control"
                    id="email"
                    name="email"
                    placeholder="Masukan Email"
                    autofocus />
                </div>
                <div class="mb-3 form-password-toggle">
                  <div class="d-flex justify-content-between">
                    <label class="form-label" for="password">Password</label>
                    <a href="#">
                      <small>Lupa Password?</small>
                    </a>
                  </div>
                  <div class="input-group input-group-merge">
                    <input
                      type="password"
                      id="password"
                      class="form-control"
                      name="password"
                      placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                      aria-describedby="password" />
                    <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                  </div>
                </div>
                <div class="mb-3">
                  <button class="btn btn-primary d-grid w-100" type="submit">Login</button>
                </div>
              </form>

            </div>
          </div>
          <!-- /Register -->
        </div>
      </div>
    </div>

    

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->

    <script src="../assets/vendor/libs/jquery/jquery.js"></script>
    <script src="../assets/vendor/libs/popper/popper.js"></script>
    <script src="../assets/vendor/js/bootstrap.js"></script>
    <script src="../assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="../assets/vendor/js/menu.js"></script>
    
    @include('helpers.toast')

    <!-- endbuild -->

    <!-- Vendors JS -->

    <!-- Main JS -->
    <script src="../assets/js/main.js"></script>

    <!-- Page JS -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
  </body>
</html>

@extends('layouts.main')

@section('title', 'Form Luka')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4"><span class="text-muted fw-light">Form Luka</h4>

    <!-- Basic Layout & Basic with Icons -->
    <div class="row">
      <!-- Basic Layout -->
      <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
            Nama Pasien : {{ session('name')  }} ({{ session('phoneNumber') }})
          </div>
          <div class="card-body">
            <form method="POST" id="UploadSyaratForm" name="UploadSyaratForm" enctype="multipart/form-data">

              <input class="form-control" type="hidden" value="{{ session('id') }}" name="idUser">
              <input class="form-control" type="hidden" value="{{ session('name') }}" name="oleh">
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Tanggal Pemeriksaan</label>
                <div class="col-sm-10">
                  <input class="form-control" type="datetime-local" value="{{  now()->toDateTimeString() }}" id="html5-datetime-local-input" name="created_at">
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-email">Upload Foto</label>
                <div class="col-sm-10">
                  <div class="input-group input-group-merge">
                    <input class="form-control" type="file" id="formFile" name="picturePath">
                    
                  </div>
                 
                </div>
              </div>
              
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-message">Catatan</label>
                <div class="col-sm-10">
                  <textarea
                    id="basic-default-message"
                    class="form-control"
                    placeholder="Masukan Catatan"
                    name="catatan"
                    aria-describedby="basic-icon-default-message2"></textarea>
                </div>
              </div>
              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" id="saveSyarat" class="btn btn-primary">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- / Content -->

    <!-- Basic Layout & Basic with Icons -->
    <div class="row">
      <!-- Basic Layout -->
      <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
            Nama Pasien : {{ session('name')  }} ({{ session('phoneNumber') }})
          </div>
          <div class="card-body">
            <table id="refTabel" class="expandable-table" style="width:100%">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Gambar</th>
                      <th>Catatan</th>
                      <th>Tanggal</th>
                      <th>Aksi</th>
                  </tr>
              </thead>
          </table>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- / Content -->

  
@endsection

@push('page-stylesheet')
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
  $(function () {
    $.noConflict();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });

  let table = $('#refTabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: 'listlukapasien'
            },
            columns: [
                { data: null, orderable: false, searchable: false },
                { data: 'oleh' },
                {
                    data: 'picturePath', searchable: false,
                    render: function (data, type, row, meta) {
                        return `<a class="btn btn-outline-primary" target="_blank" href="/storage/${row['picturePath']}">Lihat</a>`;
                    },
                    className: 'text-center'
                },
                { data: 'catatan' },
                { data: 'tanggal' },
                { data: 'id' }
            ],
            'columnDefs': [
                {
                    "targets": 5,
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                            return "<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-delete' data-id='" + data + "'' data-original-title='Delete' class='btn btn-sm btn-danger deleteUser' >Hapus</a>";
                    }
                }
            ]

        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i+1;
            });
        });

        $('#refTabel').on('click', '.deleteUser', function () {

var Customer_id = $(this).data("id");
Swal.fire({
      icon: 'question',
      title: 'Apakah akan menghapus Data Luka ?',
      showCancelButton: true,
      cancelButtonText:'Tidak',
      confirmButtonText: 'Ya',
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
      $.ajax({
          type: "DELETE",
          url: "hapusluka"+'/'+Customer_id,
          success: function (data) {
                table.ajax.url('listlukapasien').load();
              Swal.fire(data.success, '', 'success')
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });

  } else if (result.isDenied) {
      Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
  }
})


});


$('#UploadSyaratForm').submit(function (e) {
        e.preventDefault();
        $('#saveSyarat').html('Mengirim Data ..');
      
        $.ajax({
          data: new FormData(this),
          url: "simpanluka",
          method: "POST",
          dataType: 'json',
          contentType:false,
          cache:false,
          processData:false,
          success: function (data) {
              $('#UploadSyaratForm').trigger("reset");
              Swal.fire(
                  data.success,
                  'Bukti Foto Luka Berhasil Disimpan..',
                  'success'
              ).then(function (result) {
              if (result.value) {
                table.ajax.url('listlukapasien').load();
              }
          })
              
              $('#saveSyarat').html('Simpan');
      
          },
          error: function (data) {
              //console.log('Error:', data);
              Swal.fire(
                  'Terdapat Kesalahan',
                  data.responseJSON.message,
                  'error'
              )
              $('#saveSyarat').html('Simpan');
          }
      });
    });

 


  });


    

  


</script>
@endpush

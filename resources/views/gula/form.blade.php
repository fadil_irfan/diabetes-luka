@extends('layouts.main')

@section('title', 'Form Gula')

@section('content')
<!-- Content -->

<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4"><span class="text-muted fw-light">Form Gula</h4>

    <!-- Basic Layout & Basic with Icons -->
    <div class="row">
      <!-- Basic Layout -->
      <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
           Nama Pasien : {{ session('name')  }} ({{ session('phoneNumber') }})
          </div>
          <div class="card-body">
            <form id="MabaForm" name="MabaForm">
              
              <input class="form-control" type="hidden" value="{{ session('id') }}" name="idUser">
              <input class="form-control" type="hidden" value="{{ session('name') }}" name="oleh">
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Tanggal Pemeriksaan</label>
                <div class="col-sm-10">
                  <input class="form-control" type="datetime-local" value="{{  now()->toDateTimeString() }}" id="html5-datetime-local-input" name="created_at">
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-name">Waktu Pemeriksaan</label>
                <div class="col-sm-10">
                  <select class="form-select" id="exampleFormControlSelect1" aria-label="Default select example" name="waktu">
                    <option value="Pagi">Pagi</option>
                    <option value="Siang">Siang</option>
                    <option value="Sore">Sore</option>
                  </select>
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-company">Jenis Pemeriksaan</label>
                <div class="col-sm-10">
                  <select class="form-select" id="exampleFormControlSelect1" aria-label="Default select example" name="jenisPemeriksaan">
                    <option value="Gula Darah Sewaktu">Gula Darah Sewaktu</option>
                    <option value="Gula Darah Puasa">Gula Darah Puasa</option>
                    <option value="GD 2 Jam Setelah Makan">GD 2 Jam Setelah Makan</option>
                    <option value="HbA1c">HbA1c</option>
                    
                  </select>
                </div>
              </div>
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-email">Nilai</label>
                <div class="col-sm-10">
                  <div class="input-group input-group-merge">
                    <input
                      type="number"
                      id="basic-default-email"
                      class="form-control"
                      placeholder="nilai"
                      name="nilai"
                       />
                    
                  </div>
                 
                </div>
              </div>
              
              <div class="row mb-3">
                <label class="col-sm-2 col-form-label" for="basic-default-message">Catatan</label>
                <div class="col-sm-10">
                  <textarea
                    id="basic-default-message"
                    class="form-control"
                    placeholder="Masukan Catatan"
                    name="catatan"
                    aria-describedby="basic-icon-default-message2"></textarea>
                </div>
              </div>
              <div class="row justify-content-end">
                <div class="col-sm-10">
                  <button type="submit" id="saveBtn" class="btn btn-primary">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- / Content -->

  <div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="py-3 mb-4"><span class="text-muted fw-light">Form Gula</h4>

    <!-- Basic Layout & Basic with Icons -->
    <div class="row">
      <!-- Basic Layout -->
      <div class="col-xxl">
        <div class="card mb-4">
          <div class="card-header d-flex align-items-center justify-content-between">
           Riwayat Data Gula Pasien : {{ session('name')  }} ({{ session('phoneNumber') }})
          </div>
          <div class="card-body">
            <table id="refTabel" class="expandable-table" style="width:100%">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Jenis Pemeriksaan</th>
                      <th>Waktu</th>
                      <th>Nilai</th>
                      <th>Catatan</th>
                      <th>Tanggal</th>
                      <th>Aksi</th>
                  </tr>
              </thead>
          </table>
          </div>
        </div>
      </div>
      
    </div>
  </div>
  <!-- / Content -->
@endsection

@push('page-stylesheet')
@endpush

@push('page-script')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript">
  $(function () {
    $.noConflict();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
  });

  let table = $('#refTabel').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: {
                url: 'listgulapasien'
            },
            columns: [
                { data: null, orderable: false, searchable: false },
                { data: 'oleh' },
                { data: 'jenisPemeriksaan' },
                { data: 'waktu' },
                { data: 'nilai' },
                { data: 'catatan' },
                { data: 'tanggal' },
                { data: 'id' }
            ],
            'columnDefs': [
                {
                    "targets": 7,
                    "className": "text-center",
                    "render": function (data, type, row, meta) {
                            return "<a href='javascript:void(0)' data-toggle='modal' data-target='#modal-delete' data-id='" + data + "'' data-original-title='Delete' class='btn btn-sm btn-danger deleteUser' >Hapus</a>";
                    }
                }
            ]

        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i+1;
            });
        });

    $('#saveBtn').click(function (e) {
        e.preventDefault();
        $('#saveBtn').html('Mengirim Data ..');

        $.ajax({
          data: $('#MabaForm').serialize(),
          url: "/simpangula",
          type: "POST",
          dataType: 'json',
          success: function (data) {
              if (data.status=='200')
            {
                $('#MabaForm').trigger("reset");
              Swal.fire(
                  'Tambah Data Pasien Berhasil',
                  'Silahkan klik tombol OK ' + data.success,
                  'success'
              ).then(function (result) {
              if (result.value) {
                table.ajax.url('listgulapasien').load();
              }
          })
            }else
            {
                Swal.fire(
                  'Terdapat Kesalahan',
                  data.error,
                  'error'
              )
            }
              $('#saveBtn').html('Simpan');

      
          },
          error: function (data) {
              console.log('Error:', data);
              Swal.fire(
                  'Terdapat Kesalahan',
                  data.responseJSON.message,
                  'error'
              )
              $('#saveBtn').html('Simpan');
          }
      });
    });


    $('#refTabel').on('click', '.deleteUser', function () {

var Customer_id = $(this).data("id");
Swal.fire({
      icon: 'question',
      title: 'Apakah akan menghapus data gula  ?',
      showCancelButton: true,
      cancelButtonText:'Tidak',
      confirmButtonText: 'Ya',
}).then((result) => {
  /* Read more about isConfirmed, isDenied below */
  if (result.isConfirmed) {
      $.ajax({
          type: "DELETE",
          url: "hapusgula"+'/'+Customer_id,
          success: function (data) {
                table.ajax.url('listgulapasien').load();
              Swal.fire(data.success, '', 'success')
          },
          error: function (data) {
              console.log('Error:', data);
          }
      });

  } else if (result.isDenied) {
      Swal.fire('Tidak Terjadi Perubahan Data', '', 'info')
  }
})


});


  });

 
</script>
@endpush
